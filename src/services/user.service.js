import { authHeader, handleResponse } from './index';

export const userService = {
    getAll
};

const {
    REACT_APP_JWT
} = process.env;

function getAll() {
    const requestOptions = { method: 'GET', headers: authHeader() };
    return fetch(`${REACT_APP_JWT}/users`, requestOptions).then(handleResponse);
}