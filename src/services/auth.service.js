import {handleResponse} from './handle-response'
import { getCustomerService, postCustomerService } from './request.service';
import { BehaviorSubject } from 'rxjs';
import { argon2i, argon2Verify } from 'hash-wasm';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));

const { REACT_APP_JWT } = process.env;

// dec2hex :: Integer -> String
function dec2hex(dec) {
    return dec.toString(16).padStart(2, "0")
}

function generateId(len) {
    var arr = new Uint8Array((len || 40) / 2)
    window.crypto.getRandomValues(arr)
    return Array.from(arr, dec2hex).join('')
}

const salt = generateId();

export const authenticationService = {
    login,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue() {
        return currentUserSubject.value
    }
};

async function login(login, password) {
    const authService = new AuthService();
    const userInstance = await authService.findUser(login)

    let userInstancePassword = '';

    if (userInstance[0]?.password)
        userInstancePassword = await authService.verifyPassword(password, userInstance[0]?.password)
    else
        return false

    if (userInstancePassword) {
        const userInstanceId = userInstance[0]?.id
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: userInstanceId,
            })
        };

        return fetch(`${REACT_APP_JWT}users/authenticate`, requestOptions)
            .then(handleResponse)
            .then(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                currentUserSubject.next(user);

                return user;
            });
    } else {
        return false
    }
}

class AuthService {
    findUser(login) {
        return getCustomerService(`?filter[login][_eq]=${login}`)
            .then((user) => {
                return user;
            });
    }
    logout() {
        // remove user from local storage to log user out
        localStorage.clear();
        currentUserSubject.next(null);
        window.location.reload()
    }
    register(login, password, name, firstname, email, salt, address) {
        return postCustomerService({
            login,
            password,
            name,
            firstname,
            email,
            salt,
            address
        })
    }
    cryptPassword(password, givenSalt) {
        let saltToUse = ''

        if (givenSalt === undefined)
            saltToUse = salt
        else
            saltToUse = givenSalt

        return {
            crypt: argon2i({
                password,
                salt: saltToUse, // salt is a buffer containing random bytes
                parallelism: 8,
                iterations: 8,
                memorySize: 4096, // use for memory
                hashLength: 32, // output for size
                outputType: 'encoded',
            }),
            salt: saltToUse
        }
    }
    async verifyPassword(password, hash) {
        return await argon2Verify({
            password,
            hash,
        });
    }
    getCurrentUser() {
        return JSON.parse(localStorage.getItem('user'));;
    }
}
export default new AuthService();