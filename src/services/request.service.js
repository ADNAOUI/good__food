const axios = require("axios");

const { REACT_APP_DIRECTUS } = process.env;

const directusCollections = axios.create({
    baseURL: `${REACT_APP_DIRECTUS}`,
    headers: {
        "Content-type": "application/json",
    }
});

/* ********************************** DIRECTUS COLLECTIONS ********************************** */
/* *************** GET ******************* */
export const getCustomerService = (params = "") => {

    return directusCollections.get(`gf_customers${params}`).then(res => {
        return res.data.data
    });
}

export const getProductsService = (params = "") => {

    return directusCollections.get(`gf_products${params}`).then(res => {
        return res.data.data
    });
}

export const getCategoriesService = (params = "") => {

    return directusCollections.get(`gf_categories${params}`).then(res => {
        return res.data.data
    });
}

export const getRestaurantsService = (params = "") => {

    return directusCollections.get(`gf_restaurants${params}`).then(res => {
        return res.data.data
    });
}

export const getCouponsService = (params = "") => {

    return directusCollections.get(`gf_coupons${params}`).then(res => {
        return res.data.data
    });
}

/* *************** POST ******************* */
export const postOrderService = (params = "") => {

    return directusCollections.post(`gf_orders`, params)
}

export const postCustomerService = (params = "") => {

    return directusCollections.post(`gf_customers`, params)
}

/* *************** PATCH ******************* */
export const patchCustomerService = (params = "", datas = "") => {

    return directusCollections.patch(`gf_customers/${params}`, datas)
}

/* *************** DELETE ******************* */
export const deleteCustomerService = (params = "") => {

    return directusCollections.delete(`gf_customers/${params}`)
}