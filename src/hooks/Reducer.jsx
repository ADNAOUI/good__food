import React, { useContext, createContext } from "react";
import createPersistedReducer from 'use-persisted-reducer';
import { toast } from 'react-toastify';

const usePersistedReducer = createPersistedReducer('cart');
const CartStateContext = createContext();
const CartDispatchContext = createContext();

const reducer = (state, action) => {
    const product = action.item
    const cartStorage = JSON.parse(localStorage.getItem('cart'))

    switch (action.type) {
        case "ADD":

            let existingItemInCart = cartStorage.find(cartItem => cartItem.id === product.id);

            toast.dark(`${typeof action.qty !== 'undefined' ? `${action.qty} produits ajoutés` : "1 produit ajouté"} à votre panier !`)

            if (state.includes(product) || existingItemInCart) {
                state.map(item => {
                    if (item.id === product.id) {
                        if (typeof action.qty !== 'undefined') {
                            item.qty = item.qty + action.qty
                        }
                        else {
                            item.qty++
                        }
                    }
                    return item
                })
            }
            else {
                if (typeof action.qty !== 'undefined') {
                    product.qty = action.qty
                }
                else {
                    product.qty = 1
                }
                state.push(product)
            }

            return [...state]

        case "REMOVE":
            const cart = [...state];
            cart.splice(action.index, 1);
            return cart;

        case "CLEAR":
            const cartRemove = [...state = []];
            return cartRemove;

        case "DECREMENT":
            return state.map(item => {
                if (item.id === product.id) {
                    item.qty--
                }
                return item;
            })

        default:
            throw new Error(`action ${action.type} est inconnue`);
    }
};

export const CartProvider = ({ children }) => {
    const [state, dispatch] = usePersistedReducer(reducer, []);

    return (
        <CartDispatchContext.Provider value={dispatch}>
            <CartStateContext.Provider value={state}>
                {children}
            </CartStateContext.Provider>
        </CartDispatchContext.Provider>
    );
};

export const useCart = () => useContext(CartStateContext);
export const useDispatchCart = () => useContext(CartDispatchContext);
