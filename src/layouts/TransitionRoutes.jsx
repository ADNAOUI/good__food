

import React, { useEffect, useState } from "react"
import { Routes, useLocation } from "react-router-dom"
import TopBarProgress from "react-topbar-progress-indicator"

TopBarProgress.config({
    barColors: {
        "0": "#F28F79",
        "1.0": "#F6B1A1"
    },
    shadowBlur: 5,
    barThickness: 2
});

const TransitionRoutes = ({ children }) => {

    const [progress, setProgress] = useState(false)
    const [prevLoc, setPrevLoc] = useState("")
    const location = useLocation()

    useEffect(() => {
        setPrevLoc(location.pathname)
        setProgress(true)
        if (location.pathname === prevLoc) {
            setPrevLoc('')
        }
    }, [location])

    useEffect(() => {
        setProgress(false)
    }, [prevLoc])

    return (
        <>
            {progress && <TopBarProgress />}
            <Routes>
                {children}
            </Routes>
        </>
    )
}

export default TransitionRoutes