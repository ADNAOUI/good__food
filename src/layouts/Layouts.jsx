import * as React from "react";

import { Route, Navigate } from "react-router-dom";
import RouteAsObj from '../routes/routes.jsx'
import TransitionRoutes from "./TransitionRoutes.jsx";

const Layouts = () => {
  return (
    <>
      <TransitionRoutes>
        <Route path="/good_food/*" element={<RouteAsObj />} />
        <Route path="/" element={<Navigate replace to="/good_food/" />} />
      </TransitionRoutes>
    </>
  );
}
export default Layouts;
