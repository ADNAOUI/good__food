import React, { useEffect, useState } from 'react';
import { useRoutes, useLocation } from "react-router"

import AuthService, { authenticationService } from '../services/auth.service';
import { CartProvider } from "../hooks/Reducer";

//Import des composants
import { Header, Footer, Cart, Error } from "../components/organisms";
import { Home, Apropos, MenusList, PromosList, DisplayRestaurants, DisplayRestaurantChild } from "../components/views";

const ScrollToTop = (props) => {
    const location = useLocation();
    useEffect(() => {
        window.scrollTo(0, 0);
    }, [location]);

    return <>{props.children}</>
};

const RouteAsObj = () => {
    const [currentUser, setCurrentUser] = useState(null)
    const routes = useRoutes([
        {
            path: "/",
            element: <Home />,
        },
        {
            // path: "contact",
            // element: <Contact />,
            // children can be used to configure nested routes
            // children: [
            //   { path: "child1", element: <Home /> },
            //   { path: "child2", element: <Footer /> },
            // ],
        },
        {
            path: "/aPropos",
            element: <Apropos />,
        },
        {
            path: "/menu",
            element: <MenusList />,
        },
        {
            path: "/promo",
            element: <PromosList />,
        },
        {
            path: "/restaurants",
            children: [
                { path: "map", element: <DisplayRestaurants /> },
                { path: ":idRestaurant", element: <DisplayRestaurantChild /> },
            ],
        },
        {
            path: "/*",
            element: <Error />,
        },
    ]);

    useEffect(() => {
        authenticationService.currentUser.subscribe(x => setCurrentUser(x));
    }, [])

    return (
        <>
            <ScrollToTop>
                <Header />
                <CartProvider>
                    <Cart />
                    <main id="main" className="">
                        {routes}
                    </main>
                </CartProvider>
                <Footer />
            </ScrollToTop>
        </>
    )
}

export default RouteAsObj