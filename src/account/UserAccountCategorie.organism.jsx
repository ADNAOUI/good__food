import React from 'react';
import { AdressesAccount, CommandsAccount, DashboardAccount, FidelityAccount, HelpsAccount, SettingsAccount } from './views';


export const UserAccountCategorie = (props) => {
     let { isDisplay } = props;

     switch (isDisplay) {
          case "dashboard":
               return (
                    <>
                        <DashboardAccount />
                    </>
               )

          case "address":
               return (
                    <>
                         <AdressesAccount />
                    </>
               )

          case "orders":
               return (
                    <>
                         <CommandsAccount />
                    </>
               )

          case"fidelity":
               return(
                    <>
                         <FidelityAccount />
                    </>
               )

          case "helps":
               return(
                    <>
                         <HelpsAccount />
                    </>
               )

          case "settings":
               return(
                    <>
                         <SettingsAccount />
                    </>
               )

          default:
               return null;
     }
}