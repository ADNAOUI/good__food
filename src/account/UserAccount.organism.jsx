import React, { useEffect, useState } from 'react';
import AuthService, { authenticationService } from '../services/auth.service';
import { IconsUser } from '../components/atoms';
import { UserAccountCategorie } from './UserAccountCategorie.organism';
import { Offcanvas } from 'react-bootstrap';
import { MdSpaceDashboard } from 'react-icons/md';

const buttonAccount = [
     {
          name: "Dashboard",
          id: "dashboard",
          className: "button-account-dashboard",
          icon: {
               reactIcons: <MdSpaceDashboard />,
               background: "#7D45C3",
          }
     },
     {
          name: "Adresses",
          id: "address",
          icon: {
               name: 'fa-solid fa-location-dot',
               background: "#BE3B3B",
          }

     },
     {
          name: "Commandes",
          id: "orders",
          icon: {
               name: 'fa-solid fa-basket-shopping',
               background: "#3B8FBE",
          }

     },
     {
          name: "Fidélité",
          id: "fidelity",
          icon: {
               name: 'fa-solid fa-tag',
               background: "#B0BA3E",
          }
     },
     {
          name: "FAQ",
          id: "helps",
          icon: {
               name: 'fa-solid fa-handshake-angle',
               background: "#B13B9F",
          }
     },
     {
          name: "Paramètres",
          id: "settings",
          icon: {
               name: 'fa-solid fa-gear',
               background: "#6A6A6A",
          }
     },
];

export const UserAccountOffCanva = (props) => {
     const [currentUser, setCurrentUser] = useState(null);
     const [componentAccounts, setComponentAccounts] = useState(false);

     const logout = () => {
          AuthService.logout();
     }

     const changeComponentsButton = (e, components) => {
          setComponentAccounts(components)
          const buttonsComponents = Array.from(document.getElementsByClassName("button-account"))

          buttonsComponents.map(button => {
               button.classList.remove("active")
          })

          e.currentTarget.classList.add("active")
     }

     useEffect(() => {

          if (window.innerWidth <= 1200) {
               setComponentAccounts("dashboard")
          }

          authenticationService.currentUser.subscribe(x => setCurrentUser(x));
     }, [])

     return (
          <>
               {(authenticationService.currentUserValue) ?
                    <>
                         <Offcanvas className="userAccountOffCanva" show={props.show} onMouseLeave={props.onHide}>
                              <Offcanvas.Header className='navigation-links header__navigation__authentication'>
                                   <div className='d-flex align-items-center header__navigation__authentication__userProfil w-100'>
                                        <IconsUser currentUser={authenticationService.currentUserValue.image} />
                                        <div className="header__navigation__authentication__userProfil__profil w-100 flex-row">
                                             <span className="header__navigation__authentication__userProfil__profil__firstname">{authenticationService.currentUserValue.firstname}&nbsp;</span>
                                             <span className="header__navigation__authentication__userProfil__profil__lastname">{authenticationService.currentUserValue.name}</span>
                                        </div>
                                   </div>
                              </Offcanvas.Header>
                              <Offcanvas.Body className='aside-left'>
                                   <aside className='userAccountOffCanva__wrapper'>
                                        <nav className='userAccountOffCanva__wrapper__content'>
                                             <ul className='userAccountOffCanva__wrapper__content__list'>
                                                  {(buttonAccount.map((button, key) => {
                                                       return (
                                                            <li key={key} className="userAccountOffCanva__wrapper__content__list__item">
                                                                 <button type="button"
                                                                      className={`button-account ${button.className ? button.className : ""}`}
                                                                      onClick={(e) => changeComponentsButton(e, button.id)}>

                                                                      <i className={button.icon.name} style={{ background: button.icon.background }}>{button.icon.reactIcons}</i>
                                                                      <span>{button.name}</span>
                                                                 </button>
                                                            </li>
                                                       )
                                                  }))}
                                             </ul>
                                        </nav>
                                        {currentUser &&
                                             <div className="userAccountOffCanva__wrapper__logout">
                                                  <button className='button-account' onClick={logout}>
                                                       <i className="fa-solid fa-arrow-right-from-bracket"></i>
                                                       <span>déconnexion</span>
                                                  </button>
                                             </div>
                                        }
                                   </aside>
                              </Offcanvas.Body>
                              {componentAccounts
                                   ? <aside className={"aside-right userAccountOffCanva__aside__" + componentAccounts}>
                                        <button className='close-account' onClick={props.onHide}>
                                             <span><i className="fa-solid fa-xmark"></i></span>
                                        </button>
                                        <UserAccountCategorie isDisplay={componentAccounts} />
                                   </aside>
                                   : null
                              }
                         </Offcanvas>
                    </>
                    :
                    null
               }
          </>
     )
}