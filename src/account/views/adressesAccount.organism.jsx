import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { authenticationService } from '../../services/auth.service';
import { SectionTableau } from '../../components/molecules';
import { date } from 'yup';

const {
    REACT_APP_DIRECTUS
} = process.env;

export const AdressesAccount = () => {
    const [dateChoiceRestaurant, setDateChoiceRestaurant] = useState([]);
    const [addressRestaurant, setAddressRestaurant] = useState([]);
    const [phoneNumber, setPhoneNumber] = useState([]);
    const [schedule, setSchedule] = useState([]);
    const [nomColonne, setNomColonne] = useState(['Date', 'Adresse', 'Téléphone', 'Horaires']);

    const [error, setError] = useState("");

    const getCustomers = () => {
        const urlRequest = `${REACT_APP_DIRECTUS}gf_customers/${authenticationService.currentUserValue.id}?fields=*,id_restaurant.*,id_restaurant.address.*`;
        Axios.get(urlRequest)
        .then(
             response => {
                  const customer = response.data.data;
                  setDateChoiceRestaurant(customer.date_choice_restaurant);
                  setAddressRestaurant(customer.id_restaurant.address.number + " " + customer.id_restaurant.address.street + " " + customer.id_restaurant.address.postal_code + " " + customer.id_restaurant.address.city);
                  setPhoneNumber(customer.id_restaurant.phone_number);
                  setSchedule(customer.id_restaurant.schedule);
            }
        )
        .catch(error => {
             setError(error);
        })
    }


    useEffect(() => {
        getCustomers();
    }, [])

    return(
        <>
            <h1 className="userAccountOffCanva__aside__address__title">Vos précédents choix de restaurants</h1>
            <p className="userAccountOffCanva__aside__address__description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu egestas mauris. Cras hendrerit semper ornare. Sed feugiat hendrerit lacus at pellentesque. Nulla eu nunc et ante tempus maximus.</p>
            <div className="container-fluid">
               <div className="row">
                    <div className="col-lg-12">
                        <SectionTableau categorie="address" legend="Date" dateChoiceRestaurant={dateChoiceRestaurant} addressRestaurant={addressRestaurant} phoneNumber={phoneNumber} schedule={schedule} nomColonne={nomColonne}/>
                    </div>
                </div>
            </div>
        </>
    )
}