import React, { useEffect, useState } from 'react';
import { Section } from '../../components/molecules';

export const HelpsAccount = () => {
    return(
        <>
            <h1 className="userAccountOffCanva__aside__fidelity__title">FAQ</h1>

            <div className="container-fluid">
               <div className="row">
                    <div className="col-lg-1" />
                    <div className="col-lg-11">
                        <Section categorie="helps" legend="Une question ?" />
                    </div>
                </div>
            </div>
        </>
    )
}