import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { authenticationService } from '../../services/auth.service';
import { Section } from '../../components/molecules';
import { deleteCustomerService } from '../../services';

const {
    REACT_APP_DIRECTUS
} = process.env;

export const SettingsAccount = () => {
    const [deliveryAddress, setDeliveryAddress] = useState([]);
    const [emailAddress, setEmailAddress] = useState([]);
    const [password, setPassword] = useState([]);
    const [payment, setPayment] = useState([]);

    const [error, setError] = useState("");

    const getCustomers = () => {
        const urlRequest = `${REACT_APP_DIRECTUS}gf_customers/${authenticationService.currentUserValue.id}?fields=address.*,*`;
        Axios.get(urlRequest)
            .then(
                response => {
                    const customer = response.data.data;
                    setDeliveryAddress(customer.address.number + " " + customer.address.street + " " + customer.address.postal_code + " " + customer.address.city);
                    setEmailAddress(customer.email);
                    setPassword(customer.password);
                    setPayment(customer.payment_card);
                }
            )
            .catch(error => {
                setError(error);
            })
    }

    const deleteCurrentCustomer = () => {
        deleteCustomerService(`${authenticationService.currentUserValue.id}`).then(res => {
            console.log(res)
            localStorage.clear()

        }).finally(() => {
            console.log("tamer")
            window.location.reload()
        })
    }

    useEffect(() => {
        getCustomers();
    }, [])

    return (
        <>
            <h1 className="userAccountOffCanva__aside__settings__title">Vos paramètres</h1>
            <div className="row">
                <div className="offset-lg-1 col-lg-5">
                    <Section categorie="settings" legend="Adresse de livraison actuelle" component={deliveryAddress} parametres="edit" />
                </div>
                <div className="col-lg-6">
                    <Section categorie="settings" legend="Mot de passe actuel" component={password} parametres="inputEdit" />
                </div>
            </div>
            <div className="row">
                <div className="offset-lg-1 col-lg-5">
                    <Section categorie="settings" legend="Adresse email actuelle" component={emailAddress} parametres="edit" />
                </div>
                <div className="col-lg-6">
                    <Section categorie="settings" legend="Mode de paiement actuel" component={payment} parametres="edit" />
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <button onClick={deleteCurrentCustomer}><i class="fa-solid fa-trash-can"></i></button>
                    {/* <Section categorie="settings" legend="Mode de paiement actuel" component={payment} parametres="edit" /> */}
                </div>
            </div>
        </>
    )
}