import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { authenticationService } from '../../services/auth.service';
import { SectionTableau } from '../../components/molecules';

const {
    REACT_APP_DIRECTUS
} = process.env;

export const CommandsAccount = () => {
    const [dateOrders, setDateOrders] = useState([]);
    const [numOrders, setNumOrders] = useState([]);
    const [priceOrders, setPriceOrders] = useState([]);
    const [nomColonne, setNomColonne] = useState(['Date', 'Commande', 'Prix TTC']);


    const [error, setError] = useState("");

    const getOrders = () => {
        const urlRequest = `${REACT_APP_DIRECTUS}gf_orders?filter[customer]=${authenticationService.currentUserValue.id}`;
        Axios.get(urlRequest)
        .then(
             response => {
                  const order = response.data.data[0];
                  setDateOrders(order.date_updated);
                  setNumOrders("N°" + order.id);
                  setPriceOrders(order.total);
            }
        )
        .catch(error => {
             setError(error);
        })
    }


    useEffect(() => {
        getOrders();
    }, [])

    return(
        <>
            <h1 className="userAccountOffCanva__aside__orders__title">Votre historique de commandes</h1>
            <p className="userAccountOffCanva__aside__orders__description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu egestas mauris. Cras hendrerit semper ornare. Sed feugiat hendrerit lacus at pellentesque. Nulla eu nunc et ante tempus maximus.</p>
            <div className="container-fluid">
               <div className="row">
                    <div>
                        <SectionTableau categorie="orders" legend="Date" dateOrders={dateOrders} numOrders={numOrders} priceOrders={priceOrders} nomColonne={nomColonne}/>
                    </div>
                </div>
            </div>
        </>
    )
}