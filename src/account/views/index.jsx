import { AdressesAccount } from "./adressesAccount.organism";
import { DashboardAccount } from "./dashboardAccount.organism";
import { CommandsAccount } from "./commandsAccount.organism";
import { FidelityAccount } from "./fidelityPointsAccount.organism";
import { HelpsAccount } from "./helpsAccount.organism";
import { SettingsAccount } from "./settingsAccount.organism";

export { AdressesAccount, DashboardAccount, CommandsAccount, FidelityAccount, HelpsAccount, SettingsAccount };
