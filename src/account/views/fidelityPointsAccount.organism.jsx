import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { authenticationService } from '../../services/auth.service';
import { Section } from '../../components/molecules';

const {
    REACT_APP_DIRECTUS
} = process.env;

export const FidelityAccount = () => {
    const [fidelityPoints, setFidelityPoints] = useState([]);

    const [error, setError] = useState("");

    const getCustomers = () => {
        const urlRequest = `${REACT_APP_DIRECTUS}gf_customers/${authenticationService.currentUserValue.id}?fields=fidelity_points`;
        Axios.get(urlRequest)
        .then(
             response => {
                  const customer = response.data.data;
                  setFidelityPoints(customer.fidelity_points);
            }
        )
        .catch(error => {
             setError(error);
        })
    }


    useEffect(() => {
        getCustomers();
    }, [])

    return(
        <>
            <h1 className="userAccountOffCanva__aside__fidelity__title">Vos points de fidélité</h1>

            <div className="container-fluid">
               <div className="row">
                    <div className="col-lg-1" />
                    <div className="col-lg-11">
                        <Section categorie="fidelity" legend="POINTS DE FIDELITE" component={fidelityPoints} />
                    </div>
                </div>
            </div>
        </>
    )
}