import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { authenticationService } from '../../services/auth.service';
import { Section } from '../../components/molecules';

const {
    REACT_APP_DIRECTUS
} = process.env;

export const DashboardAccount = () => {
    const [lastRestaurants, setLastRestaurants] = useState([]);
    const [lastOrders, setLastOrders] = useState([]);
    const [fidelityPoints, setFidelityPoints] = useState([]);
    const [promoCode, setPromoCode] = useState([]);
    const [numCustomerService, setNumCustomerService] = useState([]);

    const [error, setError] = useState("");

    const getOrders = () => {
        const urlRequest = `${REACT_APP_DIRECTUS}gf_orders?filter[customer]=${authenticationService.currentUserValue.id}`;
        Axios.get(urlRequest)
        .then(
             response => {
                  const order = response.data.data[0];
                  setLastOrders(order.date_updated + " N° " + order.id + " " + order.total);
             }
        )
        .catch(error => {
             setError(error);
        })
    }

    const getCustomers = () => {
        const urlRequest = `${REACT_APP_DIRECTUS}gf_customers/${authenticationService.currentUserValue.id}?fields=id_restaurant.address.*,fidelity_points,id_restaurant.phone_number,*`;
        Axios.get(urlRequest)
        .then(
             response => {
                  const customer = response.data.data;
                  setFidelityPoints(customer.fidelity_points);
                  setNumCustomerService(customer.id_restaurant.phone_number);
                  setLastRestaurants(customer.id_restaurant.address.number + " " + customer.id_restaurant.address.street + " " + customer.id_restaurant.address.postal_code + " " + customer.id_restaurant.address.city);
             }
        )
        .catch(error => {
             setError(error);
        })
    }

    const getCoupons = () => {
        const urlRequest = `${REACT_APP_DIRECTUS}gf_customers/${authenticationService.currentUserValue.id}?fields=coupon.name`;
        Axios.get(urlRequest)
        .then(
             response => {
                  const coupon = response.data.data;
                  setPromoCode(coupon.coupon.name);
             }
        )
        .catch(error => {
             setError(error);
        })
    }

    useEffect(() => {
        getOrders();
        getCustomers();
        getCoupons();
    }, [])

    return(
        <>
          <h1 className="userAccountOffCanva__aside__dashboard__title">Bienvenue, {authenticationService.currentUserValue.firstname}</h1>
          <div className="container-fluid">
               <div className="row">
                    <div className="col-lg-1" />
                    <div className="col-lg-5">
                         <Section categorie="dashboard" legend="Dernier restaurant enregistré" component={lastRestaurants} />
                    </div>
                    <div className="col-lg-6">
                         <Section categorie="dashboard" legend="Dernière commande effectuée" component={lastOrders} />
                    </div>
               </div>
               <div className="row">
                    <div className="col-lg-1" />
                    <div className="col-lg-5">
                         <Section categorie="dashboard_2" legend="Points de fidélité" component={fidelityPoints} />
                    </div>
                    <div className="col-lg-6">
                         <Section categorie="dashboard_2" legend="Dernier code promo reçu" component={promoCode} />
                    </div>
               </div>
               <div className="row">
                    <div className="col-lg-1" />
                    <div className="col-lg-5">
                         <Section categorie="dashboard_2" legend="Numéro service client" component={numCustomerService} />
                    </div>
               </div>
          </div>
        </>
    )
}