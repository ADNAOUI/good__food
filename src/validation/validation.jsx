import * as yup from "yup";

export const schemaValildationLogin = yup.object().shape({
  login: yup.string()
    .required("Ce champ est obligatoire"),
  password: yup.string()
    .required("Mot de passe est obligatoire"),
}).required();

export const schemaValildationRegister = yup.object().shape({
  login: yup.string()
    .min(2, "trop petit")
    .max(50, "trop long!")
    .required("Ce champ est obligatoire"),
    firstname: yup.string()
    .min(2, "trop petit")
    .max(50, "trop long!")
    .required("Ce champ est obligatoire"),
    name: yup.string()
    .min(2, "trop petit")
    .required("Ce champ est obligatoire"),
    email: yup.string()
    .email('Doit être un email valide')
    .max(255)
    .required('Ce champ est obligatoire'),
  password: yup.string()
    .required("Mot de passe est obligatoire")
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
      "Doit contenir 8 caractères, une majuscule, une minuscule, un chiffre et un caractère spécial."
    ),
  confirmPassword: yup.string()
    .required("Confirmation de mot de passe est obligatoire")
    .oneOf(
      [yup.ref("password"), null],
      "Le mot de passe de confirmation ne correspond pas"
    ),
}).required();