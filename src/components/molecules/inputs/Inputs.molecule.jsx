import React from 'react';
import { Form, FloatingLabel } from 'react-bootstrap';
import * as yup from "yup";
import { Formik, Field, ErrorMessage } from "formik";

export const Inputs = (props) => {

    return (
        <>
            <Form.Group className={props.className} controlId={props.id}>
                <div className='inputs' >
                    <Form.Control type={props.type} placeholder=" " id={props.inputId} />
                    <Form.Label htmlFor={props.inputId}>{props.label}</Form.Label>
                    <span className='which-icons'></span>
                    <span className='focus-border'></span>
                </div>
            </Form.Group>
        </>
    );
}

const validationSchema = yup.object().shape({
    firstName: yup.string()
        .min(5, "trop petit")
        .max(50, "trop long!")
        .required("Ce champ est obligatoire"),
    lastName: yup.string()
        .min(2, "trop petit")
        .max(10, "trop long!")
        .required("Ce champ est obligatoire"),
    email: yup.string()
        .email("email invalide")
        .required("l'email est obligatoire"),
    password: yup.string()
        .required("Mot de passe est obligatoire")
        .min(8, "Mot de passe doit être plus grand que 8 caractères")
        .max(50, "Mot de passe doit être plus petit que 50 caractères"),
    confirmPassword: yup.string()
        .required("Confirmation de mot de passe est obligatoire")
        .oneOf(
            [yup.ref("password"), null],
            "Le mot de passe de confirmation ne correspond pas"
        ),
    acceptTerms: yup.bool().oneOf(
        [true],
        "Accepter les conditions est obligatoire"
    )
});

const initialValues = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
    acceptTerms: false
};

const handleSubmit = (values) => {
    console.log(values);
};

export const ValidationForm = (props, e) => {
    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={(values) => handleSubmit(values)}
        >
            {({ resetForm }) => (
                <Form>
                    <div className="form-group mb-3">
                        <label htmlFor="firstName">Prénoms:</label>
                        <Field
                            type="text"
                            id="firstName"
                            name="firstName"
                            className="form-control"
                        />
                        <ErrorMessage
                            name="firstName"
                            component="small"
                            className="text-danger"
                        />
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="lastName">Nom:</label>
                        <Field
                            type="text"
                            id="lastName"
                            name="lastName"
                            className="form-control"
                        />
                        <ErrorMessage
                            name="lastName"
                            component="small"
                            className="text-danger"
                        />
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="email">Email:</label>
                        <Field
                            type="email"
                            id="email"
                            name="email"
                            className="form-control"
                        />
                        <ErrorMessage
                            name="email"
                            component="small"
                            className="text-danger"
                        />
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="password">Mot de passe:</label>
                        <Field
                            type="password"
                            id="password"
                            name="password"
                            className="form-control"
                        />
                        <ErrorMessage
                            name="password"
                            component="small"
                            className="text-danger"
                        />
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="confirmPassword">
                            Confirmer le mot de passe:
                        </label>
                        <Field
                            type="password"
                            id="confirmPassword"
                            name="confirmPassword"
                            className="form-control"
                        />
                        <ErrorMessage
                            name="confirmPassword"
                            component="small"
                            className="text-danger"
                        />
                    </div>
                    <div className="form-group form-check mb-5">
                        <Field
                            name="acceptTerms"
                            type="checkbox"
                            className="form-check-input"
                        />
                        <label htmlFor="acceptTerms" className="form-check-label">
                            J'ai lu et j'accepte les conditions
                        </label>
                        <ErrorMessage
                            name="acceptTerms"
                            component="small"
                            className="text-danger d-block"
                        />
                    </div>
                    <div className="form-group d-flex justify-content-end gap-3">
                        <button type="submit" className="btn btn-primary">
                            S'inscrire
                        </button>
                        <button
                            type="button"
                            onClick={resetForm}
                            className="btn btn-danger"
                        >
                            Annuler
                        </button>
                    </div>
                </Form>
            )}
        </Formik>
    )
}

export const TextArea = (props) => {
    return (
        <div className='focus-border'>
            <label>{props.label}</label>
            <FloatingLabel controlId="floatingTextarea2" label="Ecrire ici ...">
                <Form.Control
                    as="textarea"
                    placeholder="Leave a comment here"
                />
            </FloatingLabel>
        </div>
    )
}