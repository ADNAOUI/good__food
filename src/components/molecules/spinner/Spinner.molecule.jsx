import { Spinner } from "react-bootstrap"
import { LogoResponsive } from "../../atoms"

export const Loader = () => {
    return (
        <div className="loading">

            <LogoResponsive
                theme={"dark"}
                width={100}
                height={100} />

            <Spinner animation="border" />
        </div>
    )
}