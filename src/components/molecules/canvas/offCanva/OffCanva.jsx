import React, { useState } from 'react';
import { Offcanvas, Button, OffcanvasHeader, OffcanvasBody } from 'react-bootstrap';

export const OffCanva = (props) => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <Button variant="primary" onClick={handleShow}>
                {props.text}
            </Button>

            <Offcanvas show={show} onHide={handleClose}>
                <OffcanvasHeader closeButton>
                    <Offcanvas.Title>OffCanva</Offcanvas.Title>
                </OffcanvasHeader>
                <Offcanvas.Body>
                    Ici le texte
                </Offcanvas.Body>
            </Offcanvas>
        </>
    )
}