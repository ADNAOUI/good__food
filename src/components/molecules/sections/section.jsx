import React from 'react';
import { InitButton } from '../button/Button.molecule';
import { Accordion, Table } from 'react-bootstrap';

export const Section = (props) => {
     switch (props.categorie) {
          case "settings":
               if (props.parametres === "inputEdit") {
                    return (
                         <>
                              <div className="userAccountOffCanva__aside__section">
                                   <div className={"userAccountOffCanva__aside__section__" + props.categorie}>
                                        <span className={"userAccountOffCanva__aside__section__" + props.categorie + "__legend"}>{props.legend}</span>
                                        <div>
                                             <input type="password" value={props.component} />
                                             <InitButton className="button-cta-primary" text="modifier" />
                                        </div>
                                   </div>
                              </div>
                         </>
                    )
               } else if (props.parametres === "edit") {
                    return (
                         <>
                              <div className="userAccountOffCanva__aside__section">
                                   <div className={"userAccountOffCanva__aside__section__" + props.categorie}>
                                        <span className={"userAccountOffCanva__aside__section__" + props.categorie + "__legend"}>{props.legend}</span>
                                        <div>
                                             <div>{props.component}</div>
                                             <InitButton className="button-cta-primary" text="modifier" />
                                        </div>
                                   </div>
                              </div>
                         </>
                    )
               } else {
                    return (
                         <div>{props.component}</div>
                    )
               }

          case "fidelity":
               return (
                    <>
                         <div className="userAccountOffCanva__aside__section">
                              <div className={"userAccountOffCanva__aside__section__" + props.categorie}>
                                   <span className={"userAccountOffCanva__aside__section__" + props.categorie + "__legend"}>{props.legend}</span>
                                   {props.parametres === "input" ? <input type="password" value={props.component} /> : <div>{props.component}</div>}
                                   <div className="userAccountOffCanva__aside__section__fidelity__title">A quoi servent les points de fidélité ?</div>
                                   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum a sem et scelerisque. Suspendisse vulputate ullamcorper risus, a fringilla dolor porta sed. Sed eu rhoncus lorem, rutrum commodo enim. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse hendrerit massa vitae purus interdum, non condimentum libero aliquam. Ut aliquam, nisl eu blandit posuere, dolor eros tristique arcu, vitae vulputate lorem lacus ac erat. Fusce in euismod ligula, quis sollicitudin risus. Sed consequat justo a dictum varius. Donec maximus orci felis, quis laoreet leo tempus ut. Phasellus facilisis dui massa, nec tristique metus eleifend eu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Cras ornare turpis eu risus efficitur, non malesuada felis malesuada.</p>
                                   <div className="userAccountOffCanva__aside__section__fidelity__title">Comment utiliser les points de fidélité ?</div>
                                   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum a sem et scelerisque. Suspendisse vulputate ullamcorper risus, a fringilla dolor porta sed. Sed eu rhoncus lorem, rutrum commodo enim. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse hendrerit massa vitae purus interdum, non condimentum libero aliquam. Ut aliquam, nisl eu blandit posuere.</p>
                              </div>
                         </div>
                    </>
               );

          case "helps":
               return (
                    <>
                         <div className="userAccountOffCanva__aside__section">
                              <div className={"userAccountOffCanva__aside__section__" + props.categorie}>
                                   <span className={"userAccountOffCanva__aside__section__" + props.categorie + "__legend"}>{props.legend}</span>
                                   <div>{props.component}</div>
                                   <Accordion className={"userAccountOffCanva__aside__section__" + props.categorie + "__accordion"} defaultActiveKey="0" flush>
                                        <Accordion.Item eventKey="0">
                                             <Accordion.Header>Accordion Item #1</Accordion.Header>
                                             <Accordion.Body>
                                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                                  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                                  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                                  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                                  cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                                  est laborum.
                                             </Accordion.Body>
                                        </Accordion.Item>
                                        <Accordion.Item eventKey="1">
                                             <Accordion.Header>Accordion Item #2</Accordion.Header>
                                             <Accordion.Body>
                                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                                  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                                  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                                  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                                  cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                                  est laborum.
                                             </Accordion.Body>
                                        </Accordion.Item>
                                   </Accordion>
                              </div>
                         </div>
                    </>
               )

          default:
               return (
                    <>
                         <div className="userAccountOffCanva__aside__section">
                              <div className={"userAccountOffCanva__aside__section__" + props.categorie}>
                                   <span className={"userAccountOffCanva__aside__section__" + props.categorie + "__legend"}>{props.legend}</span>
                                   <div>{props.component}</div>
                              </div>
                         </div>
                    </>
               )
     }
}

export const SectionTableau = (props) => {
     return (
          <>
               <div className="userAccountOffCanva__aside__sectionTableau">
                    {props.categorie == "address" ?
                         <Table striped bordered responsive>
                              <thead>
                                   <tr>
                                        <th>{props.nomColonne[0]}</th>
                                        <th>{props.nomColonne[1]}</th>
                                        <th>{props.nomColonne[2]}</th>
                                        <th>{props.nomColonne[3]}</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <tr>
                                        <td>{props.dateChoiceRestaurant}</td>
                                        <td>{props.addressRestaurant}</td>
                                        <td>{props.phoneNumber}</td>
                                        <td>{props.schedule}</td>
                                   </tr>
                              </tbody>

                         </Table> :
                         <Table striped bordered responsive>
                              <thead>
                                   <tr>
                                        <th>{props.nomColonne[0]}</th>
                                        <th>{props.nomColonne[1]}</th>
                                        <th>{props.nomColonne[2]}</th>
                                   </tr>

                              </thead>
                              <tbody>
                                   <tr>
                                        <td>{props.dateOrders}</td>
                                        <td>{props.numOrders}</td>
                                        <td>{props.priceOrders}</td>
                                   </tr>

                              </tbody>
                         </Table>
                    }
               </div>
          </>
     )
}