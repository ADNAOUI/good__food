import React, { useState } from 'react';
import { InitModal } from '../../organisms';

export const InitButton = (props) => {
  return (
    <>
      <button type={props.type} id={props.id} className={props.className} onClick={props.onClick}>{props.text}</button>
    </>
  );
}

export const ButtonAddToCart = (props) => {
  return (
    <>
      <button type={props.type} className="button-cart-blob" onClick={props.onClick}>
        <i></i>
        <div className="button-cart-blob-wrapper">
          <div className="button-cart-blob-wrapper-blobs">
            <span className="button-cart-blob-wrapper-blobs-item"></span>
            <span className="button-cart-blob-wrapper-blobs-item"></span>
            <span className="button-cart-blob-wrapper-blobs-item"></span>
            <span className="button-cart-blob-wrapper-blobs-item"></span>
          </div>
        </div>
      </button>
      <div className='button-cart-blob-svg'>
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
          <defs>
            <filter id="goo">
              <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10"></feGaussianBlur>
              <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 21 -7" result="goo"></feColorMatrix>
              <feBlend in2="goo" in="SourceGraphic" result="mix"></feBlend>
            </filter>
          </defs>
        </svg>
      </div>
    </>
  );
}

export const LaunchModalButton = (props) => {
  const [modalShow, setModalShow] = useState(false);

  return (
    <>
      <InitButton type="button" id={props.idButton} className={props.className} text={props.text} onClick={() => setModalShow(true)} />
      <InitModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        content={props.content}
        id={props.id}
        size={props.size ? props.size : "xl"}
      />
    </>
  );
}

export const BurgerButton = (props) => {

  return (
    <InitButton type="button" className={props.className + " button-burger"}
      text={
        <><span></span><span></span><span></span></>
      }
      onClick={props.onClick} />
  );
}