import React from 'react';

export const h1 = (props) => {

  return (
    <>
      <h1 className={props.className}> {props.content} </h1>
    </>
  );
}

export const h2 = (props) => {

  return (
    <>
      <h2 className={props.className}> {props.content} </h2>
    </>
  );
}
