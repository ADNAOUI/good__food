import React from 'react';

const Icons = (props) => {
  return (
    <>
      <i className={"icons " + props.className}></i>
    </>
  )
}
export default Icons;