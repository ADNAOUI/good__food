import React from 'react';
import { authenticationService } from '../../../services/auth.service';

const {
  REACT_APP_DIRECTUS_IMAGES
} = process.env;

const IconsUser = (props) => {
  if (!authenticationService.currentUserValue) {
    return (
      <>
        <i className={"icons " + props.className}></i>
      </>
    )
  } else {
    return (
      <>
        <div className="header__navigation__authentication__userProfil__userPicto">
          <img src={REACT_APP_DIRECTUS_IMAGES + props.currentUser} alt="customer icon" width={50} />
        </div>
      </>
    )
  };
}
export default IconsUser;