import React from 'react';

export const ListItemsMetrics = (props) => {

    return (
        <>
            <li className= {props.className}>
                <span>{props.value}</span>
                <p>{props.text}</p>
            </li>
        </>
    );
}
