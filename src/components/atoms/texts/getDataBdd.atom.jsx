import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { authenticationService } from '../../../services/auth.service';

const {
     REACT_APP_DIRECTUS
} = process.env;

export const GetDataBdd = (props) => {
          const [error, setError] = useState("");
          const [sectionLastRestaurant, setSectionLastRestaurant] = useState([]);
          const [sectionLastCommand, setSectionLastCommand] = useState([]);

          const getDataCustomer_addresseRestaurant = () => {
               const actualRestaurantUrl = `${REACT_APP_DIRECTUS}gf_addresses/${authenticationService.currentUserValue.id_restaurant}`;
               Axios.get(actualRestaurantUrl)
               .then(
                    response => {
                         const addresse = response.data.data;
                         setSectionLastRestaurant(addresse.number + " " + addresse.street + " " + addresse.postal_code + " " + addresse.city + " " + addresse.country);
                    }
               )
               .catch(error => {
                    setError(error);
               })
          }

          const getDataCustomer_lastCommand = () => {
               const actualRestaurantUrl = `${REACT_APP_DIRECTUS}gf_orders?filter[customer][_eq]=199&sort[]=-date_created&limit=1`;
               Axios.get(actualRestaurantUrl)
               .then(
                    response => {
                         const addresse = response.data.data[0];
                         console.log(addresse.date_created);
                         return(setSectionLastCommand(addresse.date_created))
                    }
               )
               .catch(error => {
                    setError(error);
               })
          }

          useEffect(() => {
               getDataCustomer_addresseRestaurant();
               getDataCustomer_lastCommand();
          }, [])

          return (
               <>
                    <section>
                           {sectionLastRestaurant}
                    </section>
               </>
          )


}