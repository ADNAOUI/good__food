import React from 'react';
import { Figure } from 'react-bootstrap';
import Logo_Dark from '../../../assets/images/logo/logo_dark.png';
import Logo_Light from '../../../assets/images/logo/logo_light.png';
import Logo_Light_Responsive from '../../../assets/images/logo/logo_light_responsive.png';
import Logo_Dark_Responsive from '../../../assets/images/logo/logo_dark_responsive.png';

export const LogoDark = (props) => {
  return (
    <>
      <Figure className='logo'>
        <Figure.Image
          width={props.width}
          height={props.height}
          alt={props.alt}
          src={Logo_Dark}
          className='logo-dark'
        />
        <Figure.Caption>
          Nulla vitae elit libero, a pharetra augue mollis interdum.
        </Figure.Caption>
      </Figure>
    </>
  );
}

export const LogoLight = (props) => {
  return (
    <>
      <Figure className='logo'>
        <Figure.Image
          width={props.width}
          height={props.height}
          alt={props.alt}
          src={Logo_Light}
          className='logo-light'
        />
        <Figure.Caption>
          Nulla vitae elit libero, a pharetra augue mollis interdum.
        </Figure.Caption>
      </Figure>
    </>
  );
}

export const LogoResponsive = (props) => {
  return (
    <>
      <Figure className='logo'>
        <Figure.Image
          width={props.width}
          height={props.height}
          alt={props.alt}
          src={props.theme === "dark" ? Logo_Dark_Responsive : Logo_Light_Responsive}
          className='logo-responsive'
        />
      </Figure>
    </>
  );
}

export const ImagesHome = (props) => {
  return (
    <>
      <Figure className='text-center'>
        <Figure.Image
          className='image'
          width={74}
          height={74}
          alt={props.alt}
          src={props.src}
        />
        <h3>{props.title}</h3>
        <Figure.Caption>
          Donc, pour résumer, je suis souvent victime des colibris, sous-entendu des types qu’oublient toujours tout. Euh, non… Bref, tout ça pour dire, que je voudrais bien qu’on me considère en tant que Tel.
        </Figure.Caption>
      </Figure>
    </>
  )

}