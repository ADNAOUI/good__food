import React from 'react';
import CarouselApropos from '../../organisms/carousel/CarouselApropos.jsx'
import { Container, Row, Col } from 'react-bootstrap'

function Apropos(props) {
    return (
        <>
            <div className='slider'>
                <CarouselApropos />
            </div>

            <Container className='about'>

                <section className='row'>
                    <Col>
                        <h1 className='text-center mt-5'>A PROPOS</h1>
                    </Col>
                </section>

                <section className='row'>
                    <h2>Historique et présentation</h2>
                    <article>
                        <p className='paragraph__lineHeigt'>Maecenas convallis nibh est, sed porttitor nunc iaculis at. Sed elementum posuere odio, in rutrum massa pulvinar ornare. Vestibulum varius elementum fermentum. Donec eleifend pharetra quam, ac pretium augue accumsan in. Nam suscipit, libero in sagittis euismod, urna nisl commodo erat, ac pretium dolor velit in velit. Nulla imperdiet, enim ac convallis semper, ante turpis viverra lorem, ut interdum nisl est vitae ante. Vestibulum eget mauris non erat consectetur ullamcorper in a arcu. Cras ut viverra enim.</p>
                        <p className='paragraph__lineHeigt'>Donec consequat tellus quam, tempus fringilla neque viverra ac. Quisque urna enim, suscipit non feugiat id, posuere eu sem. Proin semper vulputate condimentum. Donec urna sapien, semper a mattis in, egestas sit amet nibh. Praesent ut gravida nisi, rutrum dignissim lorem. Aliquam ut diam tincidunt, luctus mauris vulputate, feugiat ipsum. Vivamus vel porta sapien. In vel aliquam nisi. Quisque mollis varius urna. Donec eget dui sit amet felis congue laoreet at nec neque. Ut quis tempor sapien. Duis posuere imperdiet lectus, quis dignissim leo aliquet vitae. Aliquam hendrerit risus et ex sodales gravida. Pellentesque cursus volutpat metus, nec tempus est pretium nec.</p>
                    </article>
                </section>

                <section className='row'>
                    <h2>Franchises et réseaux de franchisés</h2>
                    <article>
                        <p className='paragraph__lineHeigt'>Donec consequat tellus quam, tempus fringilla neque viverra ac. Quisque urna enim, suscipit non feugiat id, posuere eu sem. Proin semper vulputate condimentum. Donec urna sapien, semper a mattis in, egestas sit amet nibh. Praesent ut gravida nisi, rutrum dignissim lorem. Aliquam ut diam tincidunt, luctus mauris vulputate, feugiat ipsum. Vivamus vel porta sapien. In vel aliquam nisi. Quisque mollis varius urna. Donec eget dui sit amet felis congue laoreet at nec neque. Ut quis tempor sapien. Duis posuere imperdiet lectus, quis dignissim leo aliquet vitae. Aliquam hendrerit risus et ex sodales gravida. Pellentesque cursus volutpat metus, nec tempus est pretium nec.</p>
                        <p className='paragraph__lineHeigt'>Morbi interdum metus quis quam imperdiet volutpat. Duis ornare auctor sem, vel iaculis nisi pulvinar sed. Nunc vel augue leo. Proin semper augue quis sagittis lacinia. Nunc in vulputate odio. Aliquam id purus a purus blandit consequat vitae vitae lorem. Integer lacus dolor, pellentesque quis quam eget, molestie cursus dui.</p>
                        <p className='paragraph__lineHeigt'>Morbi ac neque ultrices, imperdiet massa vel, lobortis neque. Curabitur sit amet ipsum ante. Suspendisse tincidunt mauris eu lacus faucibus, sit amet bibendum enim cursus. Nunc eros velit, blandit venenatis gravida id, semper quis lorem. Sed velit elit, dapibus at lorem quis, placerat vestibulum libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt, turpis ut porta accumsan, urna lorem lobortis felis, quis sollicitudin orci risus ut nibh.</p>
                    </article>
                </section>

                <section className='row'>
                    <h2>Implantations géographiques</h2>
                    <article>
                        <p className='paragraph__lineHeigt'>Les établissements d’origine se situent en banlieue parisienne.
                            La croissance du groupe lui permet maintenant d’être présent dans les 20 plus grandes métropoles françaises, en Belgique et au Luxembourg.
                        </p>
                        <p className='paragraph__lineHeigt'>Donec consequat tellus quam, tempus fringilla neque viverra ac. Quisque urna enim, suscipit non feugiat id, posuere eu sem. Proin semper vulputate condimentum. Donec urna sapien, semper a mattis in, egestas sit amet nibh. Praesent ut gravida nisi, rutrum dignissim lorem. Aliquam ut diam tincidunt, luctus mauris vulputate, feugiat ipsum. Vivamus vel porta sapien. In vel aliquam nisi. Quisque mollis varius urna. Donec eget dui sit amet felis congue laoreet at nec neque. Ut quis tempor sapien. Duis posuere imperdiet lectus, quis dignissim leo aliquet vitae. Aliquam hendrerit risus et ex sodales gravida. Pellentesque cursus volutpat metus, nec tempus est pretium nec.</p>
                        <p className='paragraph__lineHeigt'>Morbi interdum metus quis quam imperdiet volutpat. Duis ornare auctor sem, vel iaculis nisi pulvinar sed. Nunc vel augue leo. Proin semper augue quis sagittis lacinia. Nunc in vulputate odio. Aliquam id purus a purus blandit consequat vitae vitae lorem. Integer lacus dolor, pellentesque quis quam eget, molestie cursus dui.</p>
                    </article>
                </section>

                <section className='row'>
                    <h2>Quelques données du groupe</h2>
                    <p >150 restaurants sur la France, la Belgique et le Luxembourg</p>
                    <p>1 millier de collaborateurs</p>
                    <p>200 recettes proposées sur l’ensemble des restaurants</p>
                    <p>1 million de repas livrés sur l’exercice 2016</p>
                    <p>La vente en ligne qui représente 80% du CA</p>
                    <p> La croissance du groupe lui permet maintenant d’être présent dans les 20 plus grandes métropoles françaises, en Belgique et au Luxembourg.
                    </p>
                </section>
            </Container>
        </>
    );
}
export default Apropos;
