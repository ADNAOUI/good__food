import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { MapContainer, TileLayer, Marker, ZoomControl } from 'react-leaflet'
import { divIcon } from "leaflet";
import { Table } from 'react-bootstrap';
import { authenticationService } from '../../../services/auth.service';

import { InitModalDialog } from './Modal';
import { ImagesHome } from '../../atoms';
import { InitButton, LaunchModalButton } from '../../molecules';

//images
import Delivery from '../../../assets/images/restaurants/free-delivery.png'
import Sales from '../../../assets/images/restaurants/flash-sale.png'
import BestResto from '../../../assets/images/restaurants/best-resto.png'
import Online from '../../../assets/images/restaurants/grocery-store.png'
import { Login } from '../../../authentication';
import { getRestaurantsService, patchCustomerService } from '../../../services';

const userConnected = authenticationService.currentUserValue

let days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

const DisplayRestaurants = () => {
    let newDate = new Date()
    let day = days[newDate.getDay() - 1];
    if (typeof day === 'undefined') {
        day = "Dimanche"
    }

    const idUserConnected = userConnected?.id
    const idUserFavoriteRestaurant = userConnected?.id_restaurant

    const [activeRestaurant, setActiveRestaurant] = useState("");
    const [data, setData] = useState([]);
    const [error, setError] = useState("");
    const [loading, setLoading] = useState(true);
    const [marker, setMarker] = useState("#F28F79")
    const [idCurrentRestaurant, setIdCurrentRestaurant] = useState("")

    const markerHtmlStyles = `background-color: ${marker}`
    const icon = new divIcon({
        className: "my-custom-marker",
        html: `<span style="${markerHtmlStyles}" />`
    })

    const changeMarkerColor = (favorite) => {
        // console.log(favorite)
        // console.log(icon.options.className.add("oui"))
        // if (icon.options.iconUrl === markerActive.options.iconUrl) {
        //     setMarker((current) => (current = markerDefault));
        // } else {
        //     setMarker((current) => (current = markerActive));
        // }
    };

    const restaurants = async () => {
        try {
            await getRestaurantsService(`?fields=*,address.*`).then(res => { setData(res) })
        } catch (error) {
            setError(error.message);
        } finally {
            setLoading(false);
        }
    }

    const addFavoriteRestaurants = async (e, favorite) => {

        e.preventDefault()
        const idFavoriteRestaurant = favorite.id

        let postFavoriteRestaurant = {
            "id_restaurant": idFavoriteRestaurant,
        }

        try {
            await patchCustomerService(idUserConnected,postFavoriteRestaurant)

            const parsedLocalStorageItems = JSON.parse(localStorage.currentUser);
            parsedLocalStorageItems.id_restaurant = idFavoriteRestaurant
            const setCustomerIdRestaurant = JSON.stringify(parsedLocalStorageItems);
            localStorage.setItem("currentUser", setCustomerIdRestaurant);

        }
        catch (error) {
            setError(error.message);
        }
        finally {
            window.location.reload()
        }
    }

    const callModalFavoriteRestaurant = () => {
        const favoriteRestaurant = data.find(restaurant => restaurant.id === idUserFavoriteRestaurant)
        setActiveRestaurant(favoriteRestaurant)
        setIdCurrentRestaurant(idUserFavoriteRestaurant)
    }

    useEffect(() => {
        restaurants()
        callModalFavoriteRestaurant()
    }, [loading])

    return (
        <>
            <section className='restaurants-title row'>
                <h1>mon restaurant</h1>
                <p>
                    Pour profiter de nos meilleurs prix, n'oubliez pas d'indiquer votre magasin favori
                </p>
            </section>

            <section className='restaurants row'>
                <MapContainer
                    className='restaurants__map'
                    center={[47.1751888, 2.9933814]}
                    zoom={6}
                    zoomControl={false}
                    scrollWheelZoom={false}
                >
                    <ZoomControl position='bottomright' />
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />

                    {!loading &&
                        data.map(restaurant => (
                            <Marker
                                key={restaurant.id}
                                position={[restaurant.address.localisation.coordinates[0], restaurant.address.localisation.coordinates[1]]}
                                eventHandlers={{
                                    click: (e) => {
                                        setActiveRestaurant(restaurant);
                                        changeMarkerColor(icon)
                                        setIdCurrentRestaurant(restaurant.id)
                                    },
                                }}
                                icon={icon}
                            >
                            </Marker>
                        ))
                    }

                    {activeRestaurant &&
                        <InitModalDialog
                            header={
                                <h3>
                                    {activeRestaurant.name}
                                </h3>
                            }
                            content={
                                <>
                                    <article className='restaurants__map__modal__body__address'>
                                        <b><i className="fa-solid fa-location-dot"></i></b>
                                        <p>
                                            <span>{activeRestaurant.address.number}&nbsp;</span>
                                            <span>{activeRestaurant.address.street}</span>
                                            <br />
                                            <span>{activeRestaurant.address.postal_code} {activeRestaurant.address.city} {activeRestaurant.address.country}</span>
                                        </p>
                                    </article>

                                    <article className='restaurants__map__modal__body__phone'>
                                        <b><i className="fa-solid fa-phone"></i></b>
                                        <p>
                                            <span>{activeRestaurant.phone_number}</span>
                                        </p>
                                    </article>

                                    <article className='restaurants__map__modal__body__schedule'>
                                        <b><i className="fa-solid fa-clock"></i></b>
                                        <div className='w-100'>
                                            <Table>
                                                <tbody>
                                                    {
                                                        days.map((item, k) => {
                                                            return (
                                                                <tr key={k} className={
                                                                    item === day ? "restaurants__map__modal__body__schedule__day" : ""
                                                                }>
                                                                    <td><span>{item}</span></td>
                                                                    <td><span>{activeRestaurant.schedule}</span></td>
                                                                </tr>
                                                            )
                                                        })
                                                    }
                                                </tbody>
                                            </Table>
                                        </div>
                                    </article>
                                </>
                            }
                            footer={
                                <section className='d-flex flex-column justify-content-center align-items-center gap-3'>
                                    {idUserConnected
                                        ?
                                        <InitButton
                                            type="button"
                                            id={`btnActiveRestaurant_${activeRestaurant.id}`}
                                            className={`button-standart ${idCurrentRestaurant === idUserFavoriteRestaurant ? "favorite-restaurant" : ""}`}
                                            onClick={(e) => addFavoriteRestaurants(e, activeRestaurant)}
                                            text={`${idCurrentRestaurant === idUserFavoriteRestaurant ? "Magasin préféré" : "Définir comme mon magasin préféré"}`}
                                        />
                                        :
                                        <LaunchModalButton type="button" id='modalAuthentication' className="button-standart"
                                            text="Définir comme mon magasin préféré"
                                            content={<Login />}
                                        />
                                    }

                                    <Link to={`/good_food/restaurants/${idCurrentRestaurant}`} className='link-cta-primary'>Voir la page restaurant</Link>
                                </section>
                            }
                        />
                    }
                </MapContainer>
            </section>

            <section className='informations-restaurants row'>
                <div className="col-3">
                    <ImagesHome
                        className=""
                        src={Delivery}
                        title='Livraison'
                    />
                </div>
                <div className="col-3">
                    <ImagesHome
                        className=""
                        src={Online}
                        title='Click & Collect'
                    />
                </div>
                <div className="col-3">
                    <ImagesHome
                        className=""
                        src={BestResto}
                        title='Prix du meilleur Restaurants'
                    />
                </div>
                <div className="col-3">
                    <ImagesHome
                        className=""
                        src={Sales}
                        title='Promotions tous les jours'
                    />
                </div>
            </section>
        </>
    );
}

export default DisplayRestaurants;