import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from 'axios'
import { Card, Col, Container, Row, Table } from 'react-bootstrap';
import { Icons } from '../../atoms';

import { MapContainer, TileLayer, Marker, ZoomControl } from 'react-leaflet'
import { divIcon } from "leaflet";
//images
import About from '../../../assets/images/restaurants/restaurant/about.jpg'
import Chef from '../../../assets/images/restaurants/restaurant/chef.jpg'
import ChooseLeft from '../../../assets/images/restaurants/restaurant/choose-left.jpg'
import ChooseRight from '../../../assets/images/restaurants/restaurant/choose-right.jpg'
import Welcome from '../../../assets/images/restaurants/restaurant/welcome.jpg'
import YearsTop from '../../../assets/images/restaurants/restaurant/years-top.jpg'
import YearsBottom from '../../../assets/images/restaurants/restaurant/years-bottom.jpg'
import { InitModalDialog } from './Modal';
import { Loader } from '../../molecules';


const { REACT_APP_DIRECTUS, REACT_APP_DIRECTUS_IMAGES } = process.env;
let days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

const DisplayRestaurantChild = () => {
    let newDate = new Date()
    let day = days[newDate.getDay() - 1];
    if (typeof day === 'undefined') {
        day = "Dimanche"
    }

    const { idRestaurant } = useParams();

    const [data, setData] = useState([]);
    const [error, setError] = useState("");
    const [loading, setLoading] = useState(true);

    const restaurant = async () => {
        try {
            const response = await axios.get(`${REACT_APP_DIRECTUS}gf_restaurants/${idRestaurant}?fields=*,address.*`);
            setData(response.data.data);
        } catch (error) {
            setError(error.message);
        } finally {
            setLoading(false);
        }
    }

    const markerHtmlStyles = `background-color: #F28F79`

    const icon = new divIcon({
        className: "my-custom-marker",
        html: `<span style="${markerHtmlStyles}" />`
    })

    useEffect(() => {
        restaurant()
    }, [])

    if (loading) {
        return <div className='d-flex justify-content-center mt-5'><Loader /></div>
    }
    else {
        return (
            <div className='restaurant'>
                <Row className='restaurant__welcome' style={{ backgroundImage: `url(${Welcome})` }}>
                    <div className='d-flex flex-column justify-content-center position-relative h-100 p-0'>
                        <section className='py-5'>
                            <h1 className='restaurant-title'>
                                Bienvenue à <span>{data.name}</span>
                            </h1>
                        </section>

                        <div className='restaurant__welcome__button'>
                            <a href='#restaurantAbout'>
                                <span><Icons className="fa-solid fa-chevron-down" /></span>
                            </a>
                        </div>
                    </div>
                </Row>

                <Row id="restaurantAbout" className='restaurant__about'>
                    <Col lg={8}>
                        <section className='restaurant__about__wrapper'>
                            <h2 className='restaurant-title'>
                                A propos de <span>{data.name} <b>Restaurant</b> <b>Qualité</b> <b>Ambiance</b></span>
                            </h2>

                            <Row className='restaurant__about__wrapper__chef'>
                                <Col lg={8}>
                                    <article className='restaurant__about__wrapper__chef__article'>
                                        <h3 className='text-center'>Restaurant Etoilés</h3>
                                        <p>
                                            (Aux jeunes chevaliers) Le Graal, c’est une vraie saloperie, méfiez-vous. Un jour c’est un vase, une semaine après une pierre incandescente. [...] Incandescente, c’est : qui peut accaparer des objets sans resurgir sur autrui.
                                            "Ça prouve que j'ai de l'ubiquité... De l'humilité ? C'est pas quand il y a des infiltrations ?"
                                        </p>
                                        <ul className='restaurant__about__wrapper__chef__article__stars'>
                                            <li>
                                                <i className="fa-solid fa-star"></i>
                                            </li>
                                            <li>
                                                <i className="fa-solid fa-star"></i>
                                            </li>
                                            <li>
                                                <i className="fa-solid fa-star"></i>
                                            </li>
                                            <li>
                                                <i className="fa-solid fa-star"></i>
                                            </li>
                                            <li>
                                                <i className="fa-solid fa-star"></i>
                                            </li>
                                        </ul>
                                    </article>
                                </Col>
                                <Col lg={4}>
                                    <img src={Chef} alt="" className='restaurant-image' />
                                </Col>
                            </Row>
                        </section>
                    </Col>
                    <Col lg={4}>
                        <img src={About} alt="" className='restaurant-image' />
                    </Col>
                </Row>

                <Row className='restaurant__informations'>
                    <h2 className='restaurant-title'><span>{data.name}</span> Informations</h2>

                    <MapContainer className='restaurant__informations__map'
                        zoomControl={false}
                        center={[data.address.localisation.coordinates[0], data.address.localisation.coordinates[1]]}
                        zoom={20}
                        scrollWheelZoom={false}>

                        <TileLayer
                            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />
                        <ZoomControl position='bottomright' />
                        <Marker
                            key={data.id}
                            position={[data.address.localisation.coordinates[0], data.address.localisation.coordinates[1]]}
                            icon={icon}
                        >
                        </Marker>
                        <InitModalDialog
                            header={<span></span>}
                            content={
                                <>
                                    <article className='restaurants__map__modal__body__address'>
                                        <b><i className="fa-solid fa-location-dot"></i></b>
                                        <p>
                                            <span>{data.address.number}&nbsp;</span>
                                            <span>{data.address.street}</span>
                                            <br />
                                            <span>{data.address.postal_code} {data.address.city} {data.address.country}</span>
                                        </p>
                                    </article>

                                    <article className='restaurants__map__modal__body__phone'>
                                        <b><i className="fa-solid fa-phone"></i></b>
                                        <p>
                                            <span>{data.phone_number}</span>
                                        </p>
                                    </article>

                                    <article className='restaurants__map__modal__body__schedule'>
                                        <b><i className="fa-solid fa-clock"></i></b>
                                        <div className='w-100'>
                                            <Table>
                                                <tbody>
                                                    {
                                                        days.map((item, k) => {
                                                            return (
                                                                <tr key={k} className={
                                                                    item === day ? "restaurants__map__modal__body__schedule__day" : ""
                                                                }>
                                                                    <td><span>{item}</span></td>
                                                                    <td><span>{data.schedule}</span></td>
                                                                </tr>
                                                            )
                                                        })
                                                    }
                                                </tbody>
                                            </Table>
                                        </div>
                                    </article>
                                </>
                            }
                        />
                    </MapContainer>
                </Row>

                <Row className='restaurant__service'>
                    <section className='restaurant__service__chef'>
                        <h2 className='restaurant-title'><span>Good Food</span> Services et Chef</h2>

                        <Row className='justify-content-center'>
                            <Col lg={{ span: 2 }}>
                                <div className='d-flex flex-column gap-5'>
                                    <div className='restaurant__service__chef__icon'>
                                        <span><Icons className="fa-solid fa-percent" /></span>
                                        <h5 className="restaurant-title">Réduction permanante</h5>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
                                    </div>
                                    <div>
                                        <div className='restaurant__service__chef__icon'>
                                            <span><Icons className="fa-solid fa-truck-fast" /></span>
                                            <h5 className="restaurant-title">Livraison rapide</h5>
                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col lg={4}>
                                <div className='d-flex justify-content-center align-items-center h-100'>
                                    <img src={`${REACT_APP_DIRECTUS_IMAGES}${data.chef}`} alt="" width={400} height={400} />
                                </div>
                            </Col>
                            <Col lg={{ span: 2 }}>
                                <div className='d-flex flex-column gap-5'>
                                    <div className='restaurant__service__chef__icon'>
                                        <span><Icons className="fa-solid fa-pepper-hot" /></span>
                                        <h5 className="restaurant-title">Des repas délicieux et pimentés !</h5>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
                                    </div>
                                    <div>
                                        <div className='restaurant__service__chef__icon'>
                                            <span><Icons className="fa-solid fa-utensils" /></span>
                                            <h5 className="restaurant-title">Des Chefs professionnels</h5>
                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>

                        </Row>
                    </section>
                    <Col lg={3}>
                        <Card>
                            <Card.Header>
                                <span><Icons className="fa-solid fa-shop" /></span>
                            </Card.Header>
                            <Card.Body>
                                <h4 className='restaurant-title'>Commande</h4>
                                <Card.Text>
                                    Some quick example text to build on the card title and make up the
                                    bulk of the card's content.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col lg={3}>
                        <Card>
                            <Card.Header>
                                <span><Icons className="fa-solid fa-utensils" /></span>
                            </Card.Header>
                            <Card.Body>
                                <h4 className='restaurant-title'>Restauration sur place</h4>
                                <Card.Text>
                                    Some quick example text to build on the card title and make up the
                                    bulk of the card's content.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col lg={3}>
                        <Card>
                            <Card.Header>
                                <span><Icons className="fa-solid fa-cake-candles" /></span>
                            </Card.Header>
                            <Card.Body>
                                <h4 className='restaurant-title'>Fête d'anniversaire</h4>
                                <Card.Text>
                                    Some quick example text to build on the card title and make up the
                                    bulk of the card's content.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>

                <Row className='restaurant__years'>
                    <Col lg={4}>
                        <img src={YearsTop} alt="" className='restaurant-image' />
                    </Col>
                    <Col lg={{ span: 7, offset: 1 }}>
                        <h2 className='restaurant-title'>
                            Il y a 20 ans <span>Good Food</span> est née
                        </h2>
                        <article className='d-flex align-items-center h-100'>
                            <p>
                                Dans le Languedoc, ils m'appellent Provençal. Mais c'est moi qui m'suis gouré en disant mon nom. Sinon, en Bretagne, c'est le Gros Faisan au sud, et au nord, c'est juste Ducon...
                                Le code c'est "le code" ? Ça va, ils se sont pas trop cassé le bonnet, pour l'trouver celui-là !
                                Si on avait bu un coup dans des trucs qui s'cassent, j'en aurais pété un par terre avant d'monter dans ma chambre, pour bien montrer comment j'suis colère.
                            </p>
                        </article>
                    </Col>
                </Row>

                <Row className='restaurant__years p-0'>
                    <Col lg={{ span: 7, offset: 5 }}>
                        <ul className='restaurant__years__stars'>
                            <li>
                                <i className="fa-solid fa-star"></i>
                            </li>
                            <li>
                                <i className="fa-solid fa-star"></i>
                            </li>
                            <li>
                                <i className="fa-solid fa-star"></i>
                            </li>
                            <li>
                                <i className="fa-solid fa-star"></i>
                            </li>
                            <li>
                                <i className="fa-solid fa-star"></i>
                            </li>
                        </ul>
                    </Col>
                </Row>

                <Row className='restaurant__years'>
                    <Col lg={4}>
                        <img src={YearsBottom} alt="" className='restaurant-image' />
                    </Col>
                    <Col lg={{ span: 7, offset: 1 }}>
                        <article className='d-flex align-items-center h-100'>
                            <Row className='w-100'>
                                <Col lg={5}>
                                    <h4 className='restaurant-title'>Des problèmes ?</h4>
                                    <p>
                                        "Ça prouve que j'ai de l'ubiquité... De l'humilité ? C'est pas quand il y a des infiltrations ?"
                                    </p>
                                </Col>
                                <Col lg={{ span: 5, offset: 2 }}>
                                    <h4 className='restaurant-title'>Les résoudres </h4>
                                    <p>
                                        "Ça prouve que j'ai de l'ubiquité... De l'humilité ? C'est pas quand il y a des infiltrations ?"
                                    </p>
                                </Col>
                            </Row>
                        </article>
                    </Col>
                </Row>

                <Row className='restaurant__choose'>
                    <Col lg={3}>
                        <img src={ChooseLeft} alt="" className='restaurant-image' />
                    </Col>
                    <Col lg={6}>
                        <section className='d-flex flex-column justify-content-center h-100'>
                            <article className='d-flex flex-column gap-5 px-5'>
                                <h2 className='restaurant-title'>
                                    Pourquoi choisir <span>Good Food</span>
                                </h2>
                                <p>
                                    (À Arthur) Moi, j'serais vous, je vous écouterais... Non, moi, j'serais nous, je vous... Si moi, j'étais vous, je vous écouterais ! Non, elle me fait chier, cette phrase !
                                    Au printemps, j’aime bien pisser du haut des remparts au lever du soleil... Y’a une belle vue !
                                </p>
                                <ul className='restaurant__choose__tag'>
                                    <li>
                                        <span>Fast Food</span>
                                    </li>
                                    <li>
                                        <span>Végétarien</span>
                                    </li>
                                    <li>
                                        <span>Desserts</span>
                                    </li>
                                </ul>
                                <nav className='restaurant__choose__nav'>
                                    <ul className='restaurant__choose__nav__social'>
                                        <li className=""><Icons className="fab fa-facebook" /></li>
                                        <li className=""><Icons className="fab fa-twitter" /></li>
                                        <li className=""><Icons className="fab fa-instagram" /></li>
                                        <li className=""><Icons className="fab fa-youtube" /></li>
                                        <li className=""><Icons className="fab fa-google" /></li>
                                    </ul>
                                </nav>
                            </article>
                        </section>

                    </Col>
                    <Col lg={3}>
                        <img src={ChooseRight} alt="" className='restaurant-image' />
                    </Col>
                </Row>
            </div>
        );
    }
}

export default DisplayRestaurantChild;