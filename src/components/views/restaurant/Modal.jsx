import React from 'react';

import { Modal } from 'react-bootstrap';

export const InitModalDialog = (props) => {
    return (
        <>
            <Modal.Dialog
                {...props}
                id={props.id}
                className="restaurants__map__modal"
            >
                <Modal.Header className='restaurants__map__modal__header'>
                    {props.header}
                </Modal.Header>
                <Modal.Body className='restaurants__map__modal__body'>
                    {props.content}
                </Modal.Body>
                <Modal.Footer className='restaurants__map__modal__footer'>
                    {props.footer}
                </Modal.Footer>
            </Modal.Dialog>
        </>
    )
}
