import { useEffect, useState } from "react";
import { loadModules } from "esri-loader";

export const useCreateMap = (mapRef) => {

    useEffect(() => {
        let view;

        const initializeMap = async (mapRef) => {

            const modules = [
                'esri/Map',
                'esri/views/MapView',
                'esri/widgets/Search',
                'esri/Graphic',
                'esri/layers/FeatureLayer'
            ]
            const [Map, MapView, Search, Graphic, FeatureLayer] = await loadModules(modules)

            const map = new Map(
                { basemap: "streets-navigation-vector" }
            )

            const searchWidget = new Search({ view: view });

            // First create a point geometry (this is the location of the Titanic)
            const point = {
                type: "point", // autocasts as new Point()
                longitude: 1.44315167067532,
                latitude: 43.59936022255866,
            };

            // Create a symbol for drawing the point
            const markerSymbol = {
                type: "simple-marker", // autocasts as new SimpleMarkerSymbol()
                color: [226, 119, 40],
                outline: {
                    // autocasts as new SimpleLineSymbol()
                    color: [255, 255, 255],
                    width: 2
                }
            };

            // Create a graphic and add the geometry and symbol to it
            const pointGraphic = new Graphic({
                geometry: point,
                symbol: markerSymbol
            });


            const popupTrailheads = {
                "title": "Trailhead",
                "content": "<b>Trail:</b> {TRL_NAME}<br><b>City:</b> {CITY_JUR}<br><b>Cross Street:</b> {X_STREET}<br><b>Parking:</b> {PARKING}<br><b>Elevation:</b> {ELEV_FT} ft"
              }

              const trailheads = new FeatureLayer({
                url: "https://services3.arcgis.com/GVgbJbqm8hXASVYi/arcgis/rest/services/Trailheads_Styled/FeatureServer/0",
                outFields: ["TRL_NAME","CITY_JUR","X_STREET","PARKING","ELEV_FT"],
                popupTemplate: popupTrailheads
              });

            view = new MapView({
                map: map,
                container: mapRef.current,
                center: [2.9933814, 47.1751888],
                zoom: 5,
            })

            // Add the graphics to the view's graphics layer
            view.graphics.addMany([pointGraphic]);
            view.ui.add(searchWidget, {
                position: "top-right"
            });
           map.add(trailheads);
        }

        initializeMap(mapRef)

        return () => view?.destroy()

    }, [mapRef]);
}
