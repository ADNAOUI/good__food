const modalInstance = () => {
    initModal({
        id: 'modalStore',
        title: 'Choisir ma boutique',
        url: stores_url + '?ajax=true', // à changer
        buttons: [{
            type: 'submit',
            text: 'Fermer',
            data: {
                dismiss: 'modal',
            },
        }, ],
    })
}

if (window.location.hash === '#boutiques-modal-call') {
    modalInstance()
}

$('.stores-modal-call--js').on('click', () => {
    modalInstance()
});

const callPopover = () => {
    $('[data-toggle="popover"]').popover()
}

const lottie = () => {
    $('#popoverUp').on('shown.bs.popover', () => {
        bodymovin.loadAnimation({
            container: document.getElementById('tocMapLottie'),
            path: '/themes/toc/json/lottie/map_location.json',
            renderer: 'svg',
            loop: true,
            autoplay: true,
            name: "Lottie map popover special TOC",
        });
    });
}

const mapStore = () => {
    var map, id;
    var map = L.map('map', {
        zoomControl: false,
    });
    var markers = [];
    var latLngs = [];
    var defaultBoutique = null;

    if (markers.length === 0) {

        var tileLayer = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        var tileLayerAttribution =
            '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
        var osm = new L.TileLayer(tileLayer, {
            attribution: tileLayerAttribution
        });
        var defaultBoutique = $('#storeAccordion').data('defaultboutique');
        map.addLayer(osm);
        //here code
        $('#modalBoutiques .card-header').map((i, element) => {
            markers.push(MakeMarker($(element).data('markerid'),
                $(element).data('latitude'),
                $(element).data('longitude')));
        });

        markers.map((marker, i) => {
            const pin = marker.markerObject;
            const store = marker.id_store

            pin.on("click", (e) => {
                OpenPanel(store);
                map.setView([pin.getLatLng().lat, pin.getLatLng().lng], 14);
            })
        })

        MapAutoCenter(defaultBoutique);
        L.control.zoom({
            position: 'topright'
        }).addTo(map);
    }

    callPopover();

    $('#storeAccordion > .card').on('show.bs.collapse', function (e) {
        var store = $(e.target).data('markerid');
        $(this).addClass("store-infos-open")
        $('.collapse.in').removeClass('in');
        $('.panel.is-open').removeClass('is-open');
        $('.panel.panel-default:not(".is-open")').each(function (i, el) {
            $(el).find("a[data-toggle]").addClass('collapsed');
        });

        $(e.target).closest('.panel').addClass('is-open');
        LocalizeMarker(store);
        if (typeof id_product !== 'undefined')
            getStock(store, id_product);
    });

    $('#storeAccordion > .card').on('hide.bs.collapse', function (e) {
        $(e.target).closest('.panel').removeClass('is-open');
        $(this).removeClass("store-infos-open")
    });

    function MapAutoCenter(defaultBoutique) {
        if (defaultBoutique) {
            LocalizeMarker(defaultBoutique);
            OpenPanel(defaultBoutique);
        } else {
            var bounds = new L.LatLngBounds(latLngs);
            map.setView(new L.LatLng(bounds.getCenter().lat, bounds.getCenter().lng), 6);
        }
    }

    function MakeMarker(id_store, lat, lng) {
        latLngs.push([lat, lng]);
        return {
            'id_store': id_store,
            'markerObject': L.marker([lat, lng]).addTo(map)
        };
    }

    function OpenPanel(id_store) {
        const boutique = $('#storeHeading-' + id_store + ':not(".in")');

        if (id_store) {
            boutique.parent().addClass("store-infos-open")
            $("#storeAccordion").find(`#storeDatas-${id_store}`).collapse('show');
        }

        $('.collapse.in').removeClass('in');
        $('.panel.is-open').removeClass('is-open');
        boutique.collapse('show');
    }

    function LocalizeMarker(id_store) {
        const marker = markers.find(marker => marker.id_store === id_store).markerObject;

        map.setView([marker.getLatLng().lat, marker.getLatLng().lng], 14);
    }

    function getStock(id_store, id_product) {
        var panelWrapper = $('#storeHeading-' + id_store + ' > .panel-available-wrapper');
        var products = [];
        var label = $('#storeHeading-' + id_store + ' .panel-available');
        var labelSuccess = '<div class="panel-available panel-available-success">' + stock_label_success +
            '</div>';
        var labelDanger = '<div class="panel-available panel-available-danger">' + stock_label_danger +
            '</div>';
        var labelLoading =
            '<div class="panel-available panel-available-loading"><img src="/themes/default-bootstrap/img/loading.svg"/></div>';
        products.push(id_product);
        if (label.length === 0) {
            panelWrapper.html('');
            panelWrapper.html(labelLoading);
            if (products.length > 0) {
                $.post(stores_url, {
                    products: products,
                    store: id_store,
                    action: "getStock",
                    ajax: true
                }, function (data) {
                    var products = JSON.parse(data);
                    $.each(products, function (id, value) {
                        if (value > 0) {
                            if (value === 1)
                                panelWrapper.html(
                                    '<div class="panel-available panel-available-success">' +
                                    value + ' ' + stock_label_success + '</div>');
                            else
                                panelWrapper.html(
                                    '<div class="panel-available panel-available-success">' +
                                    value + ' ' + stock_label_success_plural + '</div>');
                        } else {
                            panelWrapper.html(labelDanger);
                        }
                    });
                });
            }
        }
    }

    // Géolocalisation d'un individu
    document.getElementById("locateMe").addEventListener("click", ClosestMaker);

    function ClosestMaker() {
        navigator.geolocation.getCurrentPosition(function (position) {
            var closest = NearestMarker(position.coords.latitude, position.coords.longitude);
            LocalizeMarker(closest);
            id = closest;
            OpenPanel(id);
        });
    }

    function NearestMarker(latitude, longitude) {
        var mindif = 99999;
        var closest;

        for (index = 0; index < markers.length; ++index) {
            var dif = PythagorasEquirectangular(latitude, longitude, markers[index].markerObject.getLatLng()
                .lat,
                markers[index].markerObject.getLatLng().lng);
            if (dif < mindif) {
                closest = markers[index].id_store;
                mindif = dif;
            }
        }

        return closest;
    }

    // Convert Degress to Radians
    function Deg2Rad(deg) {
        return deg * Math.PI / 180;
    }

    function PythagorasEquirectangular(lat1, lon1, lat2, lon2) {
        lat1 = Deg2Rad(lat1);
        lat2 = Deg2Rad(lat2);
        lon1 = Deg2Rad(lon1);
        lon2 = Deg2Rad(lon2);
        var R = 6371; // km
        var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
        var y = (lat2 - lat1);
        var d = Math.sqrt(x * x + y * y) * R;
        return d;
    }
}