// import React from 'react';
// import axios from 'axios';
// import { argon2i, argon2Verify } from 'hash-wasm';

// import {required, vusername, email, vpassword} from '../../../validation/validation'

// import { Container, Row, Col, Button } from 'react-bootstrap';
// // import { Form } from 'react-bootstrap';

// import { Loader } from '../../molecules';
// import { CardContent, Error } from '../../organisms';

// import Form, { form } from "react-validation/build/form";
// import Input from "react-validation/build/input";
// import CheckButton from "react-validation/build/button";

// import AuthService from "../../../services/auth.service";

// //créer un salt random et l'insérer en bdd dans la table customer,
// //le récupérer et le modifier en cas d'update de données,
// //les salts doivent être tous différents --> RANDOM
// //Les deux lignes ci dessous en font des random --> à utiliser lors de la création d'un compte

// const { REACT_APP_DIRECTUS } = process.env;

// const defaultInputs = [
//   {
//     label: "login",
//     props: {
//       type: "text",
//       name: "login",
//       value: '',
//       validations: [required, vusername]
//     },
//   },
//   {
//     label: "firstname",
//     props: {
//       type: "text",
//       name: "firstname",
//       value: '',
//       validations: [required, vusername]
//     },
//   },
//   {
//     label: "name",
//     props: {
//       type: "text",
//       name: "name",
//       value: '',
//       validations: [required, vusername]
//     },
//   },
//   {
//     label: "email",
//     props: {
//       type: "text",
//       name: "email",
//       value: '',
//       validations: [required, email]
//     },
//   },
//   {
//     label: "password",
//     props: {
//       type: "password",
//       name: "password",
//       value: '',
//       validations: [required, vpassword]
//     },
//   },
// ];
// class Contact extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       ...defaultInputs,
//       login: '',
//       password: '',
//       name: '',
//       firstname: '',
//       email: '',
//       salt:'',
//       address: {
//         number: 5,
//         street: 'kcqvcqs csqijbcqs',
//         street_complement: 'sqcqscsq',
//         city: 'cqscqsc',
//         country: 'csqcqscq',
//         localisation: ''
//       },
//       successful: false,
//       message: ""
//     };

//     this.onChangeDatas = this.onChangeDatas.bind(this);
//     this.handleRegister = this.handleRegister.bind(this);
//   }

//   onChangeDatas(e) {
//     this.setState({ [e.target.name]: e.target.value });
//   }

//   handleRegister = async(e) => {
//     const passwordCrypt = AuthService.cryptPassword(this.state.password).crypt
//     const saltCrypt = AuthService.cryptPassword(this.state.password).salt

//     e.preventDefault();
//     this.setState({
//       message: "",
//       successful: false
//     });
//     this.form.validateAll();
//     if (this.checkBtn.context._errors.length === 0) {
//       AuthService.register(
//         this.state.login,
//         await passwordCrypt,
//         this.state.name,
//         this.state.firstname,
//         this.state.email,
//         saltCrypt,
//       ).then(
//         response => {
//           this.setState({
//             message: response.data.message,
//             successful: true
//           });
//         },
//         error => {
//           const resMessage =
//             (error.response &&
//               error.response.data &&
//               error.response.data.message) ||
//             error.message ||
//             error.toString();
//           this.setState({
//             successful: false,
//             message: resMessage
//           });
//         }
//       );
//     }
//   }

//   render() {
//     return (
//       <div className="col-md-12">
//         <div className="card card-container">
//           <img
//             src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
//             alt="profile-img"
//             className="profile-img-card"
//             width={100}
//           />
//           <Form
//             onSubmit={this.handleRegister}
//             ref={c => {
//               this.form = c;
//             }}
//           >
//             {!this.state.successful && (
//               <div>
//                 {defaultInputs.map((data, i) => {
//                   // console.log(this.setState = { defaultInputs })
//                   return (
//                     <div key={i} className="form-group">
//                       <label htmlFor={data.label}>{data.label}</label>
//                       <Input
//                         className="form-control"
//                         type={data.props.type}
//                         name={data.props.name}
//                         value={data.props.value}
//                         onChange={this.onChangeDatas}
//                         validations={data.props.validations}
//                       />
//                     </div>
//                   )
//                 })}
//                 <div className="form-group">
//                   <button className="btn btn-primary btn-block">Sign Up</button>
//                 </div>
//               </div>
//             )}
//             {this.state.message && (
//               <div className="form-group">
//                 <div
//                   className={
//                     this.state.successful
//                       ? "alert alert-success"
//                       : "alert alert-danger"
//                   }
//                   role="alert"
//                 >
//                   {this.state.message}
//                 </div>
//               </div>
//             )}
//             <CheckButton
//               style={{ display: "none" }}
//               ref={c => {
//                 this.checkBtn = c;
//               }}
//             />
//           </Form>
//         </div>
//       </div>
//     );
//   }
// }

// function dec2hex(dec) {
//   return dec.toString(16).padStart(2, "0")
// }

// // generateId :: Integer -> String
// function generateId(len) {
//   var arr = new Uint8Array((len || 40) / 2)
//   window.crypto.getRandomValues(arr)
//   return Array.from(arr, dec2hex).join('')
// }

// const salt = generateId();

// // class Contact extends React.Component {
// //   constructor(props) {
// //     super(props);

// //     this.state = {
// //       ressources: `${REACT_APP_DIRECTUS}/gf_customers`,
// //       ressource: [],
// //       loading: false,
// //       error: null,
// //       name: "",
// //       password: "",
// //     }

// //     this.checkName = this.checkName.bind(this);
// //     this.checkPassword = this.checkPassword.bind(this);
// //     this.handleSubmit = this.handleSubmit.bind(this);
// //   }

// //   fetchDatasRessources() {
// //     axios.get(this.state.ressources)
// //       .then(res => {
// //         const ressource = res.data.data;
// //         this.setState({ ressource });
// //       })
// //       .then(() => {
// //         this.setState({ loading: true })
// //       })
// //   }

// //    checkName(event) {
// //     this.setState({ name: event.target.value });
// //   }
// //   checkPassword = async (event) => {
// //     this.setState({ password: event.target.value })
// //   }

// //   handleSubmit = async (event) => {
// //     event.preventDefault()

// //     const passwordCrypt = await AuthService.cryptPassword(this.state.password, salt)

// //     const datasToSubmit = {
// //       address: 5,
// //       login: this.state.name,
// //       password: passwordCrypt,
// //       salt: salt,
// //       email: "oui",
// //       firstname: "john",
// //       name: "ElThor",
// //     }

// //     axios.post(this.state.ressources, datasToSubmit).then(res => {
// //       if (res.status === 200) {
// //         // window.location = "/ressources_relationnelles/accueil"
// //         return console.log(salt, passwordCrypt)
// //       }
// //     })
// //   }

// //   componentDidMount() {
// //     this.fetchDatasRessources();
// //   }

// //   render() {
// //     const { ressource, loading, error } = this.state;

// //     if (error) {
// //       return <Error />;
// //     }

// //     if (!loading) {
// //       return <Loader />
// //     }
// //     else {
// //       return (
// //         <Container>
// //           <Row>
// //             <Form onSubmit={this.handleSubmit}>
// //               <Form.Group className="mb-3" controlId="formBasicName">
// //                 <Form.Label>Email address</Form.Label>
// //                 <Form.Control type="text" placeholder="Entrer un nom" value={this.state.name} onChange={this.checkName} />
// //               </Form.Group>

// //               <Form.Group className="mb-3" controlId="formBasicPassword">
// //                 <Form.Label>Password</Form.Label>
// //                 <Form.Control type="password" placeholder="Entrer un mdp" value={this.state.password} onChange={this.checkPassword} />
// //               </Form.Group>
// //               <Button variant="info" type="submit">
// //                 Submit
// //               </Button>
// //             </Form>
// //           </Row>
// //         </Container>
// //       )
// //     }
// //   }
// // }
// export default Contact;