import React from 'react';

import { Slider, OurServices, Counters, Categories } from '../../organisms';

const Home = (props) => {
  return (
    <>
      <div className='row position-relative'>
        <Slider />
        <Categories />
        <OurServices />
        <Counters />
      </div>
    </>
  );
}

export default Home;