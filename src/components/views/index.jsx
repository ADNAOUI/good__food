
import Home from './home/Home.view';
import Contact from './contact/Contact.view';
import DisplayRestaurants from './restaurant/Restaurant.views';
import DisplayRestaurantChild from './restaurant/RestaurantChildren.views';
import MenusList from './menu/MenusList';
import PromosList from './promotion/PromosList';
import Apropos from './apropos/Apropos.organism';

export { Home, Contact, MenusList, PromosList, Apropos, DisplayRestaurants,DisplayRestaurantChild };