import React from 'react';
import { useState, useEffect } from "react";  //pour parser les données
import { Row, Container, Modal } from 'react-bootstrap';
import { CardContent } from '../';

const {
    REACT_APP_DIRECTUS, REACT_APP_DIRECTUS_IMAGES } = process.env;

function Search() {

    const [datas, setDatas] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");
    useEffect(() => {
        fetch(REACT_APP_DIRECTUS + "gf_products")
            .then((response) => response.json())
            .then((json) => setDatas(json.data));  //on récupère les éléments qui sont le tableau data
        // mettre http://127.0.0.1:8055/items/products dans le navigateur pour voir le tableau data
    }, []
    );

    const handleSearchTerm = (e) => {  // dans   e.target.value   on récupère ce qui est tapé dans la barre de recherche
        let value = e.target.value;
        // value.length > 2 && setSearchTerm(value); // à partir de 3 caractères tapés on commence la recherche
        setSearchTerm(value);
    };
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <div className="search">
                <input type="text" name="search__searchBar" id="searchBar" placeholder="Rechercher" onClick={handleShow} />
            </div>
            <Modal show={show} onHide={handleClose} size="xl">
                <div className="search">
                    <input type="text" name="search__searchBar" id="searchBar" placeholder="Saisissez un nom de produit" onChange={handleSearchTerm} />
                </div>
                <Container className='productsMenusList'>
                    <Row className='productsMenusList__rowGap'>
                        {datas.filter((value) => {
                            return value.name.toLowerCase().includes(searchTerm.toLowerCase());  // retourne le titre qui inclut ce qui est tapé dans la recherche
                        }).map((value, k) => {
                            return (
                                <div className='col-item col-12 col-md-6 col-xl-4' key={k}>
                                   <CardContent product={value}/>
                                </div>
                            )
                        }
                        )}
                    </Row>
                </Container>
            </Modal>
        </>
    );
}
export default Search
