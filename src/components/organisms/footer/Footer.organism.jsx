import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import { Icons, LogoDark, ListItemsMetrics } from '../../atoms/';
import { Inputs, InitButton } from '../../molecules';

const Footer = (props) => {
  const date = new Date().getFullYear();

  return (
    <footer id="footer" className='row footer'>

      <section className='d-flex pt-5 footer__content'>
        <Container>
          <Row className='text-center text-md-start footer__content__info'>
            <Col sm={12} md={8}>
              <div className='footer__content__info__clip'>
                <div className="footer__content__info__clip__saumon"></div>
                <div className="footer__content__info__clip__blue">
                  <h6>Envie de créer votre plat ? </h6>
                  <p>
                    <span className='click-here'>Cliquez ici&nbsp;&nbsp;
                      <span>
                        <Icons className="fa-solid fa-angles-right" />
                      </span>
                    </span>
                  </p>
                </div>
                <div className="footer__content__info__clip__green"></div>
              </div>
            </Col>

            <Col sm={12} md={4}>
              <div className='footer__content__info__about'>
                <LogoDark
                  width={152}
                  height={83}
                  alt="logo Good Food pour zone sombre"
                />

                <div className='footer__content__info__about__contact'>
                  <section>
                    <h6>Nous contacter</h6>
                    <ul className='d-flex flex-column footer__content__info__about__contact__list list-items-contact'>
                      <ListItemsMetrics
                        className=""
                        value={<i className="fa-solid fa-phone-volume"></i>}
                        text="07 75 54 43 7I" />
                      <ListItemsMetrics
                        className=""
                        value={<i className="fa-solid fa-envelope"></i>}
                        text="good_food@outlook.fr" />
                      <ListItemsMetrics
                        className=""
                        value={<i className="fa-solid fa-calendar-days"></i>}
                        text="Tous les jours de 9h à 17h" />
                      <ListItemsMetrics
                        className=""
                        value={<i className="fa-solid fa-location-dot"></i>}
                        text="817 N California Ave Chicago, IL 60622" />
                    </ul>
                  </section>

                  <span className='separator'>ou</span>

                  <section>
                    <h6>Renseignez votre adresse mail ci-dessous afin d'être contacté</h6>
                    <Inputs type="email" className="" label='Email' />
                    <InitButton type="submit" className="button-standart mt-3 float-end" text="envoyer" />
                  </section>

                </div>

                <nav className='footer__content__info__about__navigation'>
                  <ul className='d-flex flex-row justify-content-evenly footer__content__info__about__navigation__list'>
                    <li><Link to="">
                      <span><Icons className="fa-brands fa-facebook-f" /></span>
                    </Link></li>
                    <li><Link to=""><Icons className="fab fa-twitter" /></Link></li>
                    <li><Link to=""><Icons className="fab fa-instagram" /></Link></li>
                    <li><Link to=""><Icons className="fab fa-youtube" /></Link></li>
                    <li><Link to=""><Icons className="fab fa-google" /></Link></li>
                  </ul>
                </nav>
              </div>
            </Col>
          </Row>
        </Container>
      </section>

      <section className='d-flex p-4 p-md-0 footer__copyright'>
        <Container>
          <Row className='align-items-center h-100 footer__copyright__text'>
            <Col sm={12}>
              <p>Copyright &copy; {date} | All Rights Reserved by Good Food</p>
            </Col>
          </Row>
        </Container>
      </section>
    </footer>
  );
}

export default Footer;