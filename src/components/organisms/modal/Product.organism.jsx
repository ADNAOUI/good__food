import React, { useEffect, useState } from 'react';

import { Row, Col, Table } from 'react-bootstrap';
import { Loader, ButtonAddToCart } from '../../molecules/';
import { useDispatchCart } from "../../../hooks/Reducer";
import { getProductsService } from '../../../services';

const { REACT_APP_DIRECTUS_IMAGES } = process.env;

export const ProductModal = (props) => {

    const [loading, setLoading] = useState(false)
    const [product, setProduct] = useState([])
    const [productQty, setProductQty] = useState(1)

    const dispatch = useDispatchCart();

    const addToCart = (e, item, qty) => {
        dispatch({ type: "ADD", item, qty });
        e.target.classList.add("active")

        setTimeout(function () {
          e.target.classList.remove('active');
        }, 1500);
    };

    const getProductDatas = () => {

        let modalId = document.querySelector(`[id^="modalProduct"]`).id;
        modalId = modalId.substring(modalId.indexOf('_') + 1)

        getProductsService(`/${modalId}?fields=id,name,image,description,price_ttc,promo,tag&fields=nutrional.value,nutrional.gf_nutritionals_id.name,nutrional.gf_nutritionals_id.unity&fields=ingredient.gf_ingredients_id.name,ingredient.gf_ingredients_id.image&fields=category.sauce.gf_sauce_id.name,category.sauce.gf_sauce_id.image`)
            .then(res => {
                setProduct(res)
            })
            .catch(error => {
                console.log(error);
            })
            .then(() => {
                setLoading(true)
            })
    }

    if (productQty <= 0) {
        setProductQty(1);
    }

    const toggleChoiceSauce = (e) => {
       return e.target.parentElement.classList.toggle("active",e.target.checked)
    }

    useEffect(() => {
        getProductDatas()
    }, [])

    if (!loading)
        return (<div className='product__loader'><Loader /></div>)
    else
        return (
            <div className='product'>
                <Row>
                    <Col sm={12}>
                        <section className='product__header'>
                            <div className='product__header__wrapper'>
                                <h2 className='product__header__wrapper__title'>{product.name}</h2>
                                <img className='product__header__wrapper__image'
                                    src={`${REACT_APP_DIRECTUS_IMAGES}${product.image}?fit=cover`}
                                    alt={product.name}
                                    width={280}
                                    height={260} />
                            </div>
                        </section>
                    </Col>
                </Row>

                {/* Description */}
                <Row>
                    <Col sm={12}>
                        <div className="product__description">
                            <Row>
                                <Col sm={7}>
                                    <h2 className='product__description__title'>Description</h2>
                                    <article className='product__description__content'>{product.description}</article>
                                </Col>

                                <Col sm={4} className="offset-sm-1">
                                    <article className='product__description__value'>
                                        <h3 className='product__description__value__title'>Valeur nutritionnelle</h3>
                                        <Table className='product__description__value__table' hover>
                                            <tbody>
                                                {(product.nutrional.map((data, k) => {
                                                    return (
                                                        <tr key={k} className=''>
                                                            <td className=''>{data.gf_nutritionals_id.name}</td>
                                                            <td className=''>{data.value}{data.gf_nutritionals_id.unity}</td>
                                                        </tr>
                                                    )
                                                }))}
                                            </tbody>
                                        </Table>
                                    </article>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                {/* end Description */}

                {/* Composition */}
                <Row>
                    <Col sm={12}>
                        <div className='product__composition wrapper-product'>
                            <h2 className='wrapper-product-title'>Composition</h2>
                            <ul className='wrapper-product-list'>
                                {(product.ingredient.map((data, k) => {
                                    return (
                                        <li key={k} className='wrapper-product-list-item'>
                                            <img className='wrapper-product-list-item-image'
                                                src={`${REACT_APP_DIRECTUS_IMAGES}${data.gf_ingredients_id.image}?fit=cover`}
                                                alt={data.gf_ingredients_id.name}
                                                width={120} height={67} />
                                            <span className='wrapper-product-list-item-name'>{data.gf_ingredients_id.name}</span>
                                        </li>
                                    )
                                }))}
                            </ul>
                        </div>
                    </Col>
                </Row>
                {/* end Composition */}

                {/* Sauce */}
                <Row>
                    <Col sm={12}>
                        <div className='product__sauce wrapper-product'>
                            <h2 className='wrapper-product-title'>Sauce</h2>
                            <ul className='wrapper-product-list'>
                                {(product.category.sauce.map((data, k) => {
                                    return (
                                        <li key={k} className='wrapper-product-list-item'>
                                            <img className='wrapper-product-list-item-image'
                                                src={`${REACT_APP_DIRECTUS_IMAGES}${data.gf_sauce_id.image}?fit=cover`}
                                                alt={data.gf_sauce_id.name}
                                                width={120} height={67} />
                                            <span className='active-sauce'></span>
                                            <input type="checkbox" id={`sauce_${data.gf_sauce_id.image}`} name={data.gf_sauce_id.name}
                                                className="wrapper-product-list-item-input" onClick={toggleChoiceSauce}
                                            />
                                            <label htmlFor={`sauce_${data.gf_sauce_id.image}`} className="wrapper-product-list-item-label">
                                                <span>{data.gf_sauce_id.name}</span>
                                            </label>
                                        </li>
                                    )
                                }))}
                            </ul>
                        </div>
                    </Col>
                </Row>
                {/* end Sauce */}

                {/* Quantity */}
                <Row>
                    <Col sm={12}>
                        <div className="product__footer">
                            <div className='quantity-fixed'>
                                <fieldset className='product__footer__quantity'>
                                    <label htmlFor="productQty">{product.price_ttc}&nbsp;€</label>
                                    <div className='product__footer__quantity__wrapper'>
                                        <button id="productQtyPlus" className='product__footer__quantity__wrapper__plus' onClick={() => setProductQty(productQty - 1)}>
                                            <i className="fa-solid fa-minus"></i>
                                        </button>
                                        <input type="text" id="productQty" min={productQty} value={productQty} readOnly />
                                        <button id="productQtyMinus" className='product__footer__quantity__wrapper__minus' onClick={() => setProductQty(productQty + 1)}>
                                            <i className="fa-solid fa-plus"></i>
                                        </button>
                                    </div>
                                </fieldset>
                                <ButtonAddToCart type="button" onClick={(e) => addToCart(e, product, productQty)} className='button-cart' />
                            </div>

                        </div>
                    </Col>
                </Row>
                {/* end Quantity */}
            </div>
        );
}
