import React from 'react';

import { Row, Col, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import { Icons, ListItemsMetrics } from '../../atoms';
import { Inputs, TextArea, InitButton } from '../../molecules';

export const ContactModal = (props) => {

    return (
        <>
            <Row className='contact gap-3 gap-lg-0'>
                <Col sm={12} lg={5}>
                    <aside className='contact__aside gap-sm-3 gap-lg-5'>
                        <h2 className='contact__aside__title'>Nous contacter</h2>
                        <p className='contact__aside__text'>Faut arrêter ces conneries de nord et de sud ! Une fois pour toutes, le nord, suivant comment on est tourné, ça change tout !</p>

                        <ul className='contact__aside__list list-items-contact gap-sm-3 gap-lg-5'>
                            <ListItemsMetrics
                                className=""
                                value={<i className="fa-solid fa-phone-volume"></i>}
                                text="07 75 54 43 7I" />
                            <ListItemsMetrics
                                className=""
                                value={<i className="fa-solid fa-envelope"></i>}
                                text="good_food@outlook.fr" />
                            <ListItemsMetrics
                                className=""
                                value={<i className="fa-solid fa-location-dot"></i>}
                                text="817 N California Ave Chicago, IL 60622" />
                        </ul>

                        <nav className='contact__aside__navigation row'>
                            <ul className='col-6 d-flex flex-lg-column flex-xl-row justify-content-evenly align-items-center align-items-lg-start align-items-xl-center gap-lg-2 gap-xl-0 contact__aside__navigation__list'>
                                <li className=""><Link to=""><Icons className="fab fa-facebook" /></Link></li>
                                <li className=""><Link to=""><Icons className="fab fa-twitter" /></Link></li>
                                <li className=""><Link to=""><Icons className="fab fa-instagram" /></Link></li>
                                <li className=""><Link to=""><Icons className="fab fa-youtube" /></Link></li>
                                <li className=""><Link to=""><Icons className="fab fa-google" /></Link></li>
                            </ul>

                            <div className='col-6 contact__aside__navigation__clip'>
                                <div className='contact__aside__navigation__clip__blue'></div>
                                <div className='contact__aside__navigation__clip__saumon'></div>
                            </div>
                        </nav>
                    </aside>
                </Col>

                <Col sm={12} lg={7}>
                    <main className='contact__main'>
                        <Form className='d-flex flex-column justify-content-center gap-5 w-100'>
                            <Row>
                                <Inputs type="text" className="col-6" label='Prénom' />
                                <Inputs type="text" className="col-6" label='Nom de famille' />
                            </Row>

                            <Row>
                                <Inputs type="email" className="col-6" label='Email' />
                                <Inputs type="tel" className="col-6" label='Téléphone' />
                            </Row>

                            <Row>
                                <TextArea label='Votre Message' />
                            </Row>

                            <Row className='justify-content-lg-end'>
                                <Col sm={12} lg={5}>
                                    <InitButton type="submit" className="button-cta-primary contact__main__submit" text="valider" />
                                </Col>
                            </Row>
                        </Form>
                    </main>
                </Col>
            </Row>
        </>
    );
}
