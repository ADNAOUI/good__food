import { Header } from './header/Header.organism';
import { HeaderDesktop } from './header/HeaderDesktop.organism';

import { CardContent } from './card/Card.organism';
import Slider from './carousel/Carousel.organism';
import { Error } from './error/Error.organism';

import { Counters } from './home/Counters.organism';
import { OurServices } from './home/OurServices.organism';
import { Categories } from './home/Categories.organism';

import { InitModal } from './modal/Modal.organism';
import { ContactModal } from './modal/Contact.organism';
import { ProductModal } from './modal/Product.organism';

import { Cart } from './cart/Cart.organism';


import Footer from './footer/Footer.organism';

export { CardContent, Slider, InitModal, ContactModal, Cart, ProductModal, Counters, OurServices, Categories, Error, Footer, Header, HeaderDesktop };