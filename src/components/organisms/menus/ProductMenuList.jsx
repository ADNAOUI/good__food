import React, { useState, useEffect } from 'react';
import { Row } from 'react-bootstrap'
import { CardContent } from '../';
import { getProductsService } from '../../../services/'

const ProductPromoList = () => {
  const [productList, setProductList] = useState(["bidule"])
  const [load, setLoad] = useState(false)

  useEffect(() => {
    getProductsService("?filter[is_menu][_eq]=1").then(res => { setProductList(res); setLoad(true) }).finally()
  }, [])

  const menuList = load && productList.map((product, k) => {
    return (
      <div className='col-item col-12 col-md-6 col-lg-3' key={k}>
        <CardContent product={product} qty />
      </div>
    )
  })

  return (
    <div className='container productsPromosList'>
      <Row className='productsPromosList__rowGap'>
        <h1 className='gf-title-principal'>Menu</h1>
        {menuList}
      </Row>
    </div>
  )
}

export default ProductPromoList