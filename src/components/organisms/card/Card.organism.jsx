import React from 'react';
import { useDispatchCart } from "../../../hooks/Reducer";

import { Card, } from 'react-bootstrap';
import { ButtonAddToCart, LaunchModalButton } from '../../molecules';

import { ProductModal } from '../';

const { REACT_APP_DIRECTUS_IMAGES } = process.env;

export const CardContent = ({ product }) => {

  const dispatch = useDispatchCart();

  const addToCart = (e, item) => {
    dispatch({ type: "ADD", item });

    e.target.classList.add("active")

    setTimeout(function () {
      e.target.classList.remove('active');
    }, 1500);
  };

  return (
    <Card>
      <Card.Header className='card__header'>
        <div className='card__header__wrapper'>
          <div className='card__header__wrapper__image' style={{ backgroundImage: `url(${REACT_APP_DIRECTUS_IMAGES}${product.image})` }}></div>
          <Card.Title className='card__header__wrapper__title'>{product.name}</Card.Title>
        </div>
      </Card.Header>
      <Card.Body className='card__body'>
        <Card.Text className='card__body__content'>
          {product.description}
        </Card.Text>
        <div className='card__body__action'>
          <LaunchModalButton
            type="button"
            id={`modalProduct_${product.id}`}
            className="button-cta-primary"
            text="détails"
            size='lg'
            content={<ProductModal />}
          />
          <span className='card__body__action__price'>
            {product.promo > 0 ?
              <>
                <span className="productsPromosList__card__body__prixBarre">{product.price_ttc} €</span>
                <strong className="productsPromosList__card__body__prixApayer">{product.price_ttc - product.promo} €</strong>
              </>
              :
              null
            }
          </span>
          <ButtonAddToCart type="button" onClick={(e) => addToCart(e, product)} />
        </div>
      </Card.Body>
    </Card>
  )
}