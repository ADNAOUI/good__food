import React, { useEffect, useState } from 'react';
import { getCategoriesService, getProductsService } from '../../../services';

import { Container, Col } from 'react-bootstrap';
import { CardContent } from '../';
import Search from '../search/Search';

const { REACT_APP_DIRECTUS_IMAGES } = process.env;

export const Categories = () => {

    const [products, setProducts] = useState([])
    const [categories, setCategories] = useState([])
    const [error, setError] = useState(false)

    const changeProductsCategories = (e) => {
        let getIdCategories = e.target.id,
            addDesign = e.target.parentElement,
            removeDesign = Array.from(document.getElementsByClassName("categories__filter__item"));

        getIdCategories = getIdCategories.substring(0, getIdCategories.indexOf('_'))

        getProductsService(`?filter[is_menu][_eq]=0&limit=8&filter[category][_eq]=${getIdCategories}`).then(res => { setProducts(res) }).catch(error => { setError(error) })

        //add & remove css class on click
        removeDesign.map(e => { return e.classList.remove("is-active") })
        addDesign.classList.add("is-active")
    }

    useEffect(() => {
        getCategoriesService().then(res => { setCategories(res) }).catch(error => { setError(error) });
        getProductsService("?filter[is_menu][_eq]=0&limit=8").then(res => { setProducts(res) })
    }, [])

    const tplCategory = categories.map((items, k) => {
        var AnimeImages = `${REACT_APP_DIRECTUS_IMAGES}${items.image}?fit=cover`

        return (
            <li key={k} className='col-item col-3 col-sm-1'>
                <div className='categories__filter__item'>
                    <img alt={items.name} src={AnimeImages} width={60} height={60} />
                    <span className='categories__filter__item__name'>{items.name}</span>
                    <span className='categories__filter__item__icon'></span>
                    <button id={`${items.id}_${items.name}`} className='categories__filter__item--change' onClick={changeProductsCategories}></button>
                </div>
            </li>
        );
    })

    const tplProduct = products.map((items, k) => {
        return (
            <li key={k} className='col-item col-12 col-md-4 col-lg-3'>
                <CardContent product={items} qty />
            </li>
        );
    })

    return (
        <Col>
            <Container className='categories'>
                <h2 className='categories__title'>Categories</h2>
                <Search />
                <ul className='row categories__filter'>
                    {tplCategory}
                </ul>
                <ul className='row categories__products'>
                    {tplProduct}
                </ul>
            </Container>
        </Col>
    );
};