import React from 'react';

import { Container } from 'react-bootstrap';
import { ListItemsMetrics } from '../../atoms';

//Import images
import Counter from '../../../assets/images/home/counters.png'

export const Counters = () => {

    return (
        <>
            <section className='counters'>
                <div className='counters__background' style={{ backgroundImage: `url(${Counter})` }}>
                </div>
                <Container className='counters__container'>
                    <ul className='row counters__container__list list-items-metrics'>
                        <ListItemsMetrics
                            className="col-12 col-sm-6 col-lg-3"
                            value="56,789"
                            text="customers served" />
                        <ListItemsMetrics
                            className="col-12 col-sm-6 col-lg-3"
                            value="56,789"
                            text="customers served" />
                        <ListItemsMetrics
                            className="col-12 col-sm-6 col-lg-3"
                            value="56,789"
                            text="customers served" />
                        <ListItemsMetrics
                            className="col-12 col-sm-6 col-lg-3"
                            value="56,789"
                            text="customers served" />
                    </ul>
                </Container>
            </section>
        </>
    );
};