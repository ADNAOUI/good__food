import React from 'react';

import { Container, Row, Col, } from 'react-bootstrap';
import { ImagesHome } from '../../atoms';

//Import images
import Background from '../../../assets/images/home/background.png'
import Truck from '../../../assets/images/home/camion.png'
import Chef from '../../../assets/images/home/chef.png'
import Order from '../../../assets/images/home/commande.png'
import Consult from '../../../assets/images/home/consulter.png'
import Diner from '../../../assets/images/home/diner.png'
import Time from '../../../assets/images/home/temps.png'

export const OurServices = () => {

    return (
        <>
            <section className='our-services' style={{ backgroundImage: `url(${Background})` }}>
                <Container>
                    <Row>
                        <Col sm={12}>
                            <h2 className='our-services__title'>nos services</h2>
                            <p className='text-center our-services__text'>Une fois, à une exécution, je m'approche d'une fille. Pour rigoler, je lui fais : « Vous êtes de la famille du pendu ? »... C'était sa sœur. Bonjour l'approche !</p>
                        </Col>
                    </Row>
                    <ul className='row mt-5 our-services__list'>
                        <li className="col-12 col-sm-6 col-lg-4">
                            <ImagesHome
                                className=""
                                src={Consult}
                                title='Consultation'
                            />
                        </li>
                        <li className="col-12 col-sm-6 col-lg-4">
                            <ImagesHome
                                className=""
                                src={Chef}
                                title='Chefs'
                            />
                        </li>
                        <li className="col-12 col-sm-6 col-lg-4">
                            <ImagesHome
                                className=""
                                src={Truck}
                                title='Livaison'
                            />
                        </li>
                        <li className="col-12 col-sm-6 col-lg-4">
                            <ImagesHome
                                className=""
                                src={Time}
                                title='Rapide'
                            />
                        </li>
                        <li className="col-12 col-sm-6 col-lg-4">
                            <ImagesHome
                                className=""
                                src={Order}
                                title='Appel téléphonique'
                            />
                        </li>
                        <li className="col-12 col-sm-6 col-lg-4">
                            <ImagesHome
                                className=""
                                src={Diner}
                                title='Commande en ligne'
                            />
                        </li>
                    </ul>
                </Container>
            </section>
        </>
    );
};