import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Offcanvas, Table, Image, OverlayTrigger, Popover, Tooltip, Alert, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { ToastContainer } from 'react-toastify';
import { Login } from '../../../authentication';
import { useCart, useDispatchCart } from '../../../hooks/Reducer';
import { getRestaurantsService, patchCustomerService, postOrderService, getCouponsService } from '../../../services';
import { authenticationService } from '../../../services/auth.service';
import { InitButton, Inputs, LaunchModalButton, Loader } from '../../molecules';
import PaymentCart from './Payment.organism';

const { REACT_APP_DIRECTUS_IMAGES } = process.env;
const userConnected = authenticationService.currentUserValue

const idUserFavoriteRestaurant = userConnected?.id_restaurant

const CartBody = ({ product, index, handleRemove }) => {

    return (
        <tr>
            <td className='cart__table__body__product d-flex align-items-center'>
                <Image className=""
                    src={`${REACT_APP_DIRECTUS_IMAGES}${product.image}`}
                    alt=""
                    roundedCircle
                    width={50}
                    height={50}
                />
                <section className="">
                    <h3>{product.name}</h3>
                    <strong>
                        {product.price_ttc.toLocaleString("fr", {
                            style: "currency",
                            currency: "EUR"
                        })}
                    </strong>
                </section>
            </td>
            <td className='cart__table__body__quantity'><span>{product.qty}</span></td>
            <td className='cart__table__body__price'>
                {(product.price_ttc * product.qty).toLocaleString("fr", {
                    style: "currency",
                    currency: "EUR"
                })}
            </td>
            <td className='cart__table__body__delete'>
                <button onClick={() => handleRemove(index)}><i className="fa-solid fa-xmark"></i></button>
            </td>
        </tr>
    );
};

export const Cart = () => {

    const [show, setShow] = useState(false);
    const [alert, setAlert] = useState(false)
    const [promo, setPromo] = useState(0)
    const [namePromo, setNamePromo] = useState()
    const [userLogged, setUserLogged] = useState(userConnected?.id ? userConnected.id : false)
    const [emptyCart, setEmptyCart] = useState(false)
    const [loading, setLoading] = useState(false)
    const [radioValue, setRadioValue] = useState('2');
    const [restaurantCostHT, setRestaurantCostHT] = useState()
    const [restaurantCostTTC, setRestaurantCostTTC] = useState()

    const handleClose = () => setShow(false);
    const handleShow = () => { setShow(true); restaurantCostDelivery() };

    const items = useCart();
    const dispatch = useDispatchCart();

    const checkPromotions = (e) => {
        e.preventDefault();
        let checkValue = document.getElementById("checkPromotions").value;

        if (checkValue) {
            getCouponsService(`?filter[name][_eq]=${checkValue}`)
                .then(res => {
                    const data = res[0]?.name
                    const idCoupon = res[0]?.id

                    if (checkValue === data) {
                        document.querySelector("#checkPromotions")?.classList.remove("is-invalid")
                        setNamePromo(idCoupon)
                        setPromo(res[0].value)
                    }
                    else {
                        document.querySelector("#checkPromotions")?.classList.add("is-invalid")
                        setPromo(0)
                    }
                })
        }
        else {
            document.getElementById("checkPromotions").classList.add("is-invalid")
        }
    }

    const displayCartPayment = () => {
        document.getElementById("cartPayment").classList.add("d-block")
        document.getElementById("cartPayment").classList.remove("d-none")
    }

    const handleRemove = (index) => {
        dispatch({ type: "REMOVE", index });
    };

    const resetCart = () => {
        dispatch({ type: "CLEAR" });
    }

    const hideCartPayment = () => {
        document.getElementById("cartPayment").classList.remove("d-block")
        document.getElementById("cartPayment").classList.add("d-none")
    }

    const restaurantCostDelivery = () => {
        if (!idUserFavoriteRestaurant) return

        getRestaurantsService(`/${idUserFavoriteRestaurant}`).then(res => {
            setRestaurantCostHT(res.delivery_cost_ttc)
            setRestaurantCostTTC(res.delivery_cost_ht)

            if (typeof res.delivery_cost_ttc === "undefined")
                setRestaurantCostTTC(0)
        })
    }

    const validateCart = (e) => {
        e.preventDefault()

        const bankCardUser = document.getElementById("cartBankCardUser").value

        if (emptyCart) {
            return false
        }

        if (bankCardUser.length !== 19) {
            document.getElementById("cartBankCardUser").classList.add("is-invalid")
            return false
        }

        const itemsCart = items.map(item => {
            let [arrayItems] = [
                {
                    "gf_products_id": item.id,
                    "quantity": item.qty,
                    "price_ht": item.price_ht,
                    "price_ttc": item.price_ttc,
                    "delivery_cost_ht": 5,
                    "delivery_cost_ttc": 7
                }
            ]

            return arrayItems
        })

        const productItemsCart =
        {
            "type": radioValue,
            "delivery_cost_ht": restaurantCostHT,
            "delivery_cost_ttc": restaurantCostTTC,
            "coupon": namePromo,
            "restaurant": idUserFavoriteRestaurant,
            "customer": userConnected.id,
            "product": itemsCart,
            "total": totalPrice
        }

        axios.all([
            postOrderService(productItemsCart),
            patchCustomerService(`${userConnected.id}?fields=payment_card`, { "payment_card": bankCardUser })
        ])
            .then(axios.spread((respPost, respPatch) => {
                if (respPost.status === 200) {
                    localStorage.removeItem("cart")
                    dispatch({ type: "CLEAR" });
                    setAlert(true)
                    setLoading(true)
                    setTimeout(function () {
                        setAlert(false)
                        setLoading(false)
                        setShow(false)
                        setRadioValue(2)
                        setPromo(0)
                    }, 2000);
                }
            }))
            .catch(error => {
                console.error('There was an error!', error);
            });
    }

    const totalPrice = (items.reduce((total, b) => total + ((b.price_ttc) * b.qty), 0) - promo + (radioValue == 1 ? restaurantCostTTC : 0)).toLocaleString("fr", {
        style: "currency",
        currency: "EUR"
    })

    const totalQuantity = items.reduce((quantity, b) => quantity + b.qty, 0)

    const tplCartBody = items.map((item, index) => (
        <CartBody
            key={index}
            product={item}
            index={index}
            handleRemove={handleRemove}
        />
    ))

    useEffect(() => {
        if (typeof restaurantCostTTC === "undefined")
            setRestaurantCostTTC(0)

        if (items.length === 0)
            setEmptyCart(true)
        else
            setEmptyCart(false)

    }, [restaurantCostTTC, items])

    return (
        <>
            <div className='display-cart'>
                <button className='button-cart'>
                    <span className='button-cart-length'>{totalQuantity}</span>
                </button>

                <InitButton className='display-cart-button' text="panier" onClick={handleShow} />
                <span className='display-cart-price'>
                    {totalPrice}
                </span>

                <ToastContainer
                    className="display-cart-toast"
                    position="top-right"
                    autoClose={2500}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    limit={1}
                />
            </div>

            <Offcanvas id="canvasCart" className="cart"
                show={show} onHide={handleClose}
                placement={'end'}>

                <Offcanvas.Header closeButton></Offcanvas.Header>
                <Offcanvas.Body>
                    {
                        loading ?
                            <section className='d-flex justify-content-center align-items-center flex-column'>
                                <div className='d-flex align-items-center justify-content-center vw-30'><Loader /></div>
                                <div className='cart__success w-100'>
                                    <Alert show={alert} variant="success" >
                                        <Alert.Heading>Votre commande est validée !</Alert.Heading>
                                        <p>  Elle est d'ores et déjà dans nos fourneaux !</p>
                                    </Alert>
                                </div>
                            </section>
                            :
                            <>
                                <aside id="cartPreview">
                                    <h2>Mon Panier</h2>

                                    <Table borderless hover className='cart__table'>
                                        <thead className='cart__table__header'>
                                            <tr>
                                                <th>Produits</th>
                                                <th>Quantité</th>
                                                <th>Prix</th>
                                                <th className='text-center'><i className="fa-solid fa-trash-can"></i></th>
                                            </tr>
                                        </thead>

                                        <tbody className='cart__table__body'>
                                            {items.length === 0 ? <tr><td colSpan={4} className='text-center'>Panier vide</td></tr> : tplCartBody}
                                        </tbody>

                                        <tfoot className='cart__table__footer'>
                                            <tr>
                                                <th scope="row">Promotion</th>
                                                <td colSpan={3}>
                                                    <form className='cart__table__footer__promotion' onSubmit={checkPromotions}>
                                                        <Inputs type="text" label='un code promo ?' inputId="checkPromotions" />
                                                        <InitButton
                                                            type="submit"
                                                            className="cart__table__footer__promotion--is-valid button-standart"
                                                            text={promo ? <span className='cart__table__footer__promotion--is-valid-correct'><i className="fa-solid fa-check"></i></span> : <span className='cart__table__footer__promotion--is-valid-default'>valider</span>}></InitButton>
                                                    </form>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colSpan={4}>
                                                    <section className='cart__table__footer__order-type'>
                                                        <h2 className='cart__table__footer__order-type__title'>Type de commande souhaitée</h2>
                                                        <div>
                                                            <ToggleButtonGroup type="radio" className='w-100 gap-5' name="options" defaultValue={2}>
                                                                <ToggleButton id="orderDelivery" className='radio-order delivery' value={1}
                                                                    checked={radioValue === 2}
                                                                    onChange={(e) => setRadioValue(e.currentTarget.value)}
                                                                >
                                                                    <span><i className="fa-solid fa-truck-fast"></i></span>
                                                                    <span>en Livraison</span>
                                                                </ToggleButton>

                                                                <ToggleButton id="orderToTake" className='radio-order taken' value={2}
                                                                    checked={radioValue === 2}
                                                                    onChange={(e) => setRadioValue(e.currentTarget.value)}
                                                                >
                                                                    <span>
                                                                        <i className="fa-solid fa-handshake-angle"></i>
                                                                    </span>
                                                                    <span>à emporter</span>
                                                                </ToggleButton>
                                                            </ToggleButtonGroup>
                                                        </div>
                                                    </section>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Total TTC</th>
                                                <td colSpan={3} className="text-center">
                                                    <b>
                                                        {(totalPrice)}
                                                    </b>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </Table>

                                    <div className='text-center mt-5 mb-5 d-flex flex-column'>
                                        {userLogged ?
                                            <InitButton className='button-cta-tertiary' text="continuer"
                                                onClick={displayCartPayment} />
                                            :
                                            <LaunchModalButton type="button" id='modalAuthentication' className="button-cta-tertiary"
                                                text="continuer"
                                                content={<Login />} />
                                        }

                                        <OverlayTrigger
                                            trigger="focus"
                                            placement="top"
                                            overlay={
                                                <Popover>
                                                    <Popover.Header as="h3">Voulez-vous supprimer votre panier en cours ? </Popover.Header>
                                                    <Popover.Body>
                                                        <div className='button-cart-reset'>
                                                            <InitButton className='button button-cart-reset--cancel' text="annuler" />
                                                            <InitButton className='button button-cart-reset--destroy' text="supprimer" onClick={resetCart} />
                                                        </div>
                                                    </Popover.Body>
                                                </Popover>
                                            }
                                        >
                                            <button className='button button-popover-cart-reset'>supprimer mon panier { }</button>
                                        </OverlayTrigger>
                                    </div>

                                    <div className='button-cart-payment'>
                                        <InitButton className='button-cart-payment-return' text="retour" onClick={hideCartPayment} />
                                    </div>

                                </aside>

                                <aside id="cartPayment">
                                    <h2>Paiement</h2>
                                    <PaymentCart />
                                    <div className='button-cart-payment'>
                                        <OverlayTrigger
                                            placement="top"
                                            overlay={
                                                <Tooltip>
                                                    {emptyCart ? "ajoutez des produits pour commander !" : "appuyez !"}
                                                </Tooltip>
                                            }
                                        >
                                            <button type="submit" className={`button-cart-payment-validate button-cta-primary ${emptyCart ? "button-disabled" : ""}`}
                                                onClick={validateCart}>commander</button>
                                        </OverlayTrigger>
                                    </div>
                                </aside>
                            </>
                    }
                </Offcanvas.Body>
            </Offcanvas>
        </>
    );
};
