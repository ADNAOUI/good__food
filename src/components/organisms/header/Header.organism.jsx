import * as React from 'react';
import { HeaderDesktop } from './HeaderDesktop.organism';

export const Header = () => {
  const [scrolling] = React.useState(100)

  function scroll() {
    const headerDesktop = document.getElementById("headerDesktop")

    document.addEventListener("scroll", () => {
      headerDesktop && headerDesktop.classList.toggle("desktop--scrolling", window.scrollY >= scrolling)
    })
  }

  React.useEffect(() => {
    scroll()
  })

  return (<HeaderDesktop />);
}
export default Header;