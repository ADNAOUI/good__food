import * as React from 'react';

import { Navigation, Icons, IconsUser, LogoResponsive } from '../../atoms';
import { BurgerButton, LaunchModalButton } from '../../molecules';
import { ContactModal } from '../';
import { Login } from '../../../authentication';
import { UserAccountOffCanva } from '../../../account/UserAccount.organism';
import { authenticationService } from '../../../services/auth.service';
import { getRestaurantsService } from '../../../services';

import Logo from '../../../assets/images/logo/logo_desktop.jpg';

import { CNavbarBrand, CNavItem, CCloseButton, CNavbar, CNavbarNav, COffcanvas, COffcanvasBody, COffcanvasHeader } from '@coreui/react';

const userConnectedFavoriteRestaurant = authenticationService.currentUserValue?.id_restaurant

export const HeaderDesktop = () => {

    const [nameRestaurant, setNameRestaurant] = React.useState("restaurant")
    const [visible, setVisible] = React.useState(false)
    const [showOffCanva, setShowOffCanva] = React.useState(false);

    const getFavoriteRestaurant = () => {
        if (userConnectedFavoriteRestaurant) {
            getRestaurantsService(`/${userConnectedFavoriteRestaurant}`).then(res => { setNameRestaurant(res.name) })
        }
    }

    const closeCanvasLink = () => {
        const button = Array.from(document.querySelectorAll(".header__navigation > .nav-item"))

        button.map((item) => {
            item.addEventListener("click", () => {
                setVisible(false)
            })
        })
    }

    React.useEffect(() => {
        getFavoriteRestaurant()
        closeCanvasLink()
    }, [])

    return (
        <header id="header" className='row header'>
            <div className='header__franchisee'>
                <p>goodfood nommé l'un des lieux de travail les plus aimés de l'année.&nbsp;
                    <span>devenez franchisé</span>
                    <Icons className="fas fa-caret-down" />
                </p>
            </div>
            <CNavbar className='header-mobile' expand="xl">
                <CNavbarBrand>
                    <Navigation to="/" content={<LogoResponsive width={60} height={100} />} className="p-0" />
                </CNavbarBrand>
                <BurgerButton
                    className={visible ? "open" : "close"}
                    aria-controls="offcanvasMenu"
                    aria-label="Toggle navigation"
                    onClick={() => setVisible(!visible)}
                />
                <COffcanvas
                    id="offcanvasMenu"
                    className='header__canvas'
                    placement="start"
                    portal={false}
                    visible={visible}
                    onHide={() => setVisible(false)}>
                    <COffcanvasHeader>
                        <CCloseButton className="text-reset button-burger" onClick={() => setVisible(false)} />
                    </COffcanvasHeader>
                    <COffcanvasBody>
                        <CNavbarNav id='headerDesktop' className='header__navigation'>
                            <CNavItem className="col">
                                {(!authenticationService.currentUserValue) ?
                                    <div className='navigation-links header__navigation__authentication'>
                                        <LaunchModalButton type="button" id='modalAuthentication' className=""
                                            text={<div className='d-flex align-items-center gap-3'><Icons className="fas fa-user-circle" /><span>s'identifier</span></div>}
                                            content={<Login />} />
                                    </div> :
                                    <div className='navigation-links header__navigation__authentication' onMouseEnter={() => setShowOffCanva(true)}>
                                        <div className='header__navigation__authentication__userProfil d-flex align-items-center gap-3'>
                                            <IconsUser currentUser={authenticationService.currentUserValue.image} />
                                            <div className="header__navigation__authentication__userProfil__profil d-flex flex-column">
                                                <div>
                                                    <span className="header__navigation__authentication__userProfil__profil__firstname">{authenticationService.currentUserValue.firstname}&nbsp;</span>
                                                    <span className="header__navigation__authentication__userProfil__profil__lastname">{authenticationService.currentUserValue.name}</span>
                                                </div>
                                                <span className="header__navigation__authentication__userProfil__profil__email">{authenticationService.currentUserValue.email}</span>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </CNavItem>
                            <CNavItem className="col">
                                <Navigation to="restaurants/map" content={nameRestaurant} className="" />
                            </CNavItem>
                            <CNavItem className="col">
                                <Navigation to="menu" content="menus" className="" />
                            </CNavItem>
                            <CNavItem className="col h-0">
                                <Navigation to="/" content={<img src={Logo} alt="logo" />} className="header__navigation__logo  no-hover" />
                            </CNavItem>

                            <CNavItem className="col">
                                <Navigation to="promo" content="promos" className="" />
                            </CNavItem>
                            <CNavItem className="col">
                                <Navigation to="aPropos" content="a propos" className="" />
                            </CNavItem>
                            <CNavItem className="col">
                                <div className='navigation-links nav-item'>
                                    <LaunchModalButton type="button" id='modalContact' className="" text="contactez-nous" content={<ContactModal />} />
                                </div>
                            </CNavItem>
                        </CNavbarNav>
                        {(showOffCanva) ?
                            <UserAccountOffCanva show="{showOffCanva}" onHide={() => setShowOffCanva(true)} /> :
                            <div />
                        }
                    </COffcanvasBody>
                </COffcanvas>
            </CNavbar>
        </header>
    );
}
