import React, { useState, useEffect } from 'react';
import { Row } from 'react-bootstrap'
import { CardContent } from '../';
import { getProductsService } from '../../../services/'

const ProductPromoList = () => {
  const [productPromo, setProductPromo] = useState(["bidule"])
  const [load, setLoad] = useState(false)

  useEffect(() => {
    getProductsService("?filter[promo][_gt]=0").then(res => { setProductPromo(res); setLoad(true) })
  }, [])

  const promoList = load && productPromo.map((product, k) => {
    return (
      <div className='col-item col-12 col-md-6 col-lg-3' key={k}>
        <CardContent product={product} qty />
      </div>
    )
  })

  return (
    <div className='container productsPromosList'>
      <Row className='productsPromosList__rowGap'>
        <h1 className='gf-title-principal'>Promotions</h1>
        {promoList}
      </Row>
    </div>
  )
}
export default ProductPromoList