import React from 'react';
import Carousel from 'react-bootstrap/Carousel'
import ImgMaquette from '../../../assets/images/carousels/imgMaquette.png'

const Slider = () => {
    return (
        <>
            <Carousel id="carouselHome">
                <Carousel.Item>
                    <img
                        className="d-block w-100 p-0"
                        src={ImgMaquette} alt={"Présentation des nouveautés et offre promotionnelle en cours"}
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={ImgMaquette} alt={"Présentation des nouveautés et offre promotionnelle en cours"}
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={ImgMaquette} alt={"Présentation des nouveautés et offre promotionnelle en cours"}
                    />
                </Carousel.Item>
            </Carousel>
        </>
    )
}
export default Slider