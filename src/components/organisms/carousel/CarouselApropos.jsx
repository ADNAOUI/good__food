import Carousel from 'react-bootstrap/Carousel'
import ImgApropos from '../../atoms/images/imgRest.jpg'
import Img2Apropos from '../../atoms/images/imageRest2.png'
import Img3Apropos from '../../atoms/images/imgRest3.jpg'

const Slider = () => {
    return (
        <>
            <Carousel className='carouselSpacing'>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={ImgApropos} alt={"Présentation restaurants good food"}
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={Img2Apropos} alt={"Présentation restaurants good food"}
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={Img3Apropos} alt={"Présentation restaurants good food"}
                    />
                </Carousel.Item>
            </Carousel>
        </>
    )
}
export default Slider