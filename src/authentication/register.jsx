import React from 'react';
import { Formik, Field, ErrorMessage, Form } from "formik";

import { schemaValildationRegister } from '../validation/validation'

import AuthService from "../services/auth.service";
import { Row } from 'react-bootstrap';

const inputsRegister = [
  {
    label: "Identifiant",
    props: {
      type: "text",
      name: "login",
    },
  },
  {
    label: "Email",
    props: {
      type: "text",
      name: "email",
    },
  },
  {
    label: "Nom",
    props: {
      type: "text",
      name: "name",
    },
  },
  {
    label: "Mot de passe",
    props: {
      type: "password",
      name: "password",
    },
  },
  {
    label: "Prénom",
    props: {
      type: "text",
      name: "firstname",
    },
  },
  {
    label: "Confirmation du mot de passe",
    props: {
      type: "password",
      name: "confirmPassword",
    },
  },
];

export default class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      firstname: '',
      name: '',
      email: '',
      password: '',
      confirmPassword: '',
      salt: '',
      address: {
        number: 5,
        street: 'kcqvcqs csqijbcqs',
        street_complement: 'sqcqscsq',
        city: 'cqscqsc',
        country: 'csqcqscq',
        localisation: ''
      },

      successful: false,
      message: ""
    };
  }

  handleRegister = async (values) => {
    const passwordCrypt = AuthService.cryptPassword(values.password).crypt
    const saltCrypt = AuthService.cryptPassword(values.password).salt

    AuthService.register(
      values.login,
      await passwordCrypt,
      values.name,
      values.firstname,
      values.email,
      saltCrypt,
    ).then(
      response => {
        this.setState({
          message: response.data.message,
          successful: true
        });
      },
      error => {
        const resMessage =
          (error.response && error.response.data && error.response.data.message)
          || error.message
          || error.toString();

        this.setState({
          successful: false,
          message: resMessage,
        });
      }
    );
  }

  render(
    { successful, message } = this.state
  ) {
    return (
      <>
        <Formik
          initialValues={{ ...this.state }}
          validationSchema={schemaValildationRegister}
          onSubmit={this.handleRegister}
        >
          {({ touched, errors, isSubmitting }) => (
            <Form className="authentication__form">
              {!successful && (
                <>
                  <h1 className="text-center">Créer votre compte</h1>
                  <Row className='authentication__form__register'>
                    {inputsRegister.map((data, i) => {
                      return (
                        <div key={i} className='col-6'>
                          <div className="inputs">
                            <Field
                              type={data.props.type}
                              id={data.props.name}
                              name={data.props.name}
                              className={`form-control ${touched[data.props.name] && errors[data.props.name] ? "is-invalid" : ""
                                }`}
                              placeholder=" "
                            />
                            <label htmlFor={data.label}>{data.label}</label>
                            <span className='which-icons'></span>
                            <span className='focus-border'></span>

                            <ErrorMessage
                              name={data.props.name}
                              component="small"
                              className="text-danger position-absolute"
                            />
                          </div>
                        </div>
                      )
                    })}
                    <div className="form-group text-center">
                      <button
                        type="submit"
                        className="button-cta-secondary">S'inscrire</button>
                    </div>
                  </Row>
                </>

              )}

              {message && (
                <div className="form-group">
                  <div className="alert alert-danger" role="alert">
                    {this.state.message}
                  </div>
                </div>
              )}

              {successful && (
                <div className="form-group">
                  <div className="alert alert-success text-center" role="alert">Bienvenue sur Good Food</div>
                  <p>Connectez-vous pour accéder à votre espace personnel</p>
                </div>
              )}
            </Form>
          )}

        </Formik>
      </>
    );
  }
}