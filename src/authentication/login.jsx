import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import { Formik, Field, ErrorMessage, Form } from "formik";

import { schemaValildationLogin } from '../validation/validation'
import { authenticationService } from "../services/auth.service";
import Register from "./register";

import { LogoResponsive } from "../components/atoms/";

const inputsLogin = [
    {
        label: "Identifiant",
        props: {
            type: "text",
            name: "login",
            value: '',
        },
    },
    {
        label: "Mot de passe",
        props: {
            type: "password",
            name: "password",
            value: '',
        },
    },
]
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: "",
            password: "",

            callRegisterModal: false,
            loading: false,
            message: ""
        };

        this.changeModalContent = this.changeModalContent.bind(this);
    }

    changeModalContent = () => {
        this.setState(toggleState => ({ callRegisterModal: !toggleState.callRegisterModal }));
    }

    handleSubmit = (values) => {
        authenticationService.login(values.login, values.password)
            .then(
                (data) => {
                    if (!data) {
                        this.setState({
                            loading: false,
                            message: "identifiant ou mot de passe incorrect"
                        });
                    }
                    else {
                        window.location.reload();
                    }
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
                    this.setState({
                        loading: false,
                        message: resMessage
                    });
                }
            );
    }

    render(
        { callRegisterModal, } = this.state
    ) {
        return (
            <Row className="authentication">
                <div className="authentication__logo">
                    <LogoResponsive theme={callRegisterModal ? null : "dark"} width={100} height={100} />
                </div>

                <Col sm={12} lg={8} className={`slide ${callRegisterModal ? "slide-register" : ""}`}>
                    {!callRegisterModal ?
                        <Formik
                            initialValues={{ ...this.state }}
                            validationSchema={schemaValildationLogin}
                            onSubmit={this.handleSubmit}
                        >
                            {({ touched, errors, isSubmitting }) => (
                                <Form className="authentication__form">
                                    <h1 className="text-center">Connectez-vous à Good Food</h1>
                                    <fieldset className="authentication__form__fieldset">
                                        {inputsLogin.map((data, i) => {
                                            return (
                                                <div key={i} className="inputs">
                                                    <Field
                                                        type={data.props.type}
                                                        id={data.props.name}
                                                        name={data.props.name}
                                                        placeholder=" "
                                                        className={`form-control ${touched[data.props.name] && errors[data.props.name] ? "is-invalid" : ""}`}
                                                    />
                                                    <label htmlFor={data.label}>{data.label}</label>
                                                    <span className='which-icons'></span>
                                                    <span className='focus-border'></span>

                                                    <ErrorMessage
                                                        name={data.props.name}
                                                        component="small"
                                                        className="text-danger position-absolute"
                                                    />
                                                </div>
                                            )
                                        })}
                                        <div className="form-group text-center">
                                            <button
                                                type="submit"
                                                className="button-cta-secondary"
                                            >
                                                {this.state.loading && (
                                                    <span className="spinner-border spinner-border-sm"></span>
                                                )}
                                                <span>Connexion</span>
                                            </button>
                                        </div>
                                    </fieldset>

                                    {this.state.message && (
                                        <div className="form-group">
                                            <div className="alert alert-danger" role="alert">
                                                {this.state.message}
                                            </div>
                                        </div>
                                    )}
                                </Form>
                            )}
                        </Formik>
                        :
                        <Register />
                    }
                </Col>

                <Col sm={12} lg={4} className={`authentication__background slide ${callRegisterModal ? "slide-login" : ""}`}>
                    {!callRegisterModal ?
                        <>
                            <article className="authentication__background__article">
                                <h1>Nouveau ici ?</h1>
                                <p>Inscrivez-vous et découvrez une grande variété de nouvelles opportunités</p>
                                <button type="button" className="button-cta-primary" onClick={this.changeModalContent}>{!callRegisterModal ? "Inscription" : "Connexion"}</button>
                            </article>
                        </>
                        :
                        <>
                            <article className="authentication__background__article">
                                <h1>Content de vous revoir !</h1>
                                <p>Pour rester en contact avec nous, veuillez vous connecter avec vos informations personnelles</p>
                                <button type="button" className="button-cta-primary" onClick={this.changeModalContent}>{!callRegisterModal ? "Inscription" : "Connexion"}</button>
                            </article>
                        </>
                    }

                    <div className="authentication__background__clip">
                        <div className={`authentication__background__clip__blue ${callRegisterModal ? "left" : "right"}`}></div>
                        <div className={`authentication__background__clip__saumon ${callRegisterModal ? "left" : "right"}`}></div>
                    </div>
                </Col>
            </Row>
        );
    }
}