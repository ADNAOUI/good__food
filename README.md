
                                                       #FRONT
-Desktop : REACT en extension .jsx
-Mobile : Solution PWA à privilégier sinon partir sur du REACT NATIVE
-SCSS : solution de travail en commun, le SCCS est plus intéressant que le CSS Natif, compilateur avec watch
Framework : Blueprint (changer de la solution bootstrap)


                                                       #BACK
Directus est le Back office du projet


                                                   #MANIERE DE DEV
##Réflexion ATOMIC
partir du plus petit pour construire le plus grand (parent-enfant)

##Convention de nommage :
1. Mettre les classes BOOTSTRAP en premier dans le code puis ensuite les classes créees
1. Les classes HTML/CSS : **BEM** (Block Element : __ Modify -- (js)) | exemple :

| **card** |                   |                          |
|----------|-------------------|--------------------------|
|          | **card**__header  |                          |
|          |                   | **card**__header__title  |
|          | **card**__content |                          |
|          |                   | **card**__content__image |
|          | **card**__footer  |                          |
|          |                   | **card**__footer–button  |


1. Les id : camelCase
1. Langues : FULL ANGLAIS ONLY


##Optimisation du code:
KISS (exemple : pas de fonction de plus de 100 lignes)
DRY
ATOMIC DESIGN (HTML/CSS au minimum)
Respects des règles W3C
-Tests OBLIGATOIRE
Respect des ATTRIBUTS HTML le plus possible → règle du W3C
Pas de style en ligne ! Pas d’attribut style=”” dans les éléments HTML.


                                                      #APPLICATION NODE JS
-Travailler uniquement avec YARN (oublier NPM)
-Extensions des fichiers en .jsx ou .tsx (mobile React Native)


                                                      #GIT => AZURE
-Travailler uniquement en SSH via Azure, pas de HTTPS
-Branches principales :
MASTER : Impossibilité de push/merge/commit sur cette branche
PREPROD : environnement de TEST commun, validation avec un maintener avant chaque merge sur MASTER
-Gestion des droits/rôles :
Maintener
Developer
Guest
-A chaque nouvelles fonctionnalités :
desktop : créer une branche en partant de DESKTOP→ OBLIGATOIRE
mobile : créer une branche en partant de MOBILE → OBLIGATOIRE
-convention de nommage :
Nom des branches : en anglais
feature/Footer : création d’une nouvelle fonctionnalité
fix/Footer : réparer un bug
-Message de commit : uniquement en anglais, interdiction des messages vides ou du “toto”
-Review Rejected : en cas de non respect des règles précédentes.

Architecture Logicielle :
API : Solution STRAPI, RESTful (gestion des requêtes get, post, put, delete)
BDD :
NOSQL : MONGO DB → lecture rapide des datas, bdd non relationnelles
SQL : MYSQL → lecture/écriture et dynamisme, bdd relationnelles parfaites pour la connexion par exemple
